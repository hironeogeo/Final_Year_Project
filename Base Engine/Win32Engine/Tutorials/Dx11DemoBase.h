#pragma once

#include <d3d11.h>
#include <d3dx11.h>
#include <DxErr.h>
#include <d3dcompiler.h>

class Dx11DemoBase
{
protected:
	HINSTANCE hInstance_;
	HWND hwnd_;

	D3D_DRIVER_TYPE driverType_;
	D3D_FEATURE_LEVEL featureLevel_;

	ID3D11Device* d3dDevice_;
	ID3D11DeviceContext* d3dContext_;
	IDXGISwapChain* swapChain_;
	ID3D11RenderTargetView* backBufferTarget_;

public:
	Dx11DemoBase();
	virtual ~Dx11DemoBase();

	bool initialise(HINSTANCE hInstance, HWND hwnd);
	void shutdown();

	virtual bool loadContent();
	virtual void unloadContent();
	bool compiled3dShader(LPCWSTR filePath, char* entry, char* shaderModel, ID3DBlob** buffer);
	
	virtual void update(float dt) = 0;
	virtual void render() = 0;
};
