#include "behaviour.h"
#include "RNG.h"

Behaviour::Behaviour(behaviours behaviourType)
{
	m_behaviourDurations[ATTACK] = DurationMinMax(2, 6);
	m_behaviourDurations[RETREAT] = DurationMinMax(3, 6);
	m_behaviourDurations[FORMUP] = DurationMinMax(2, 4);
	m_behaviourDurations[IDLE] = DurationMinMax(1, 8);
	m_behaviourDurations[DISPURSE] = DurationMinMax(2, 4);

	init(behaviourType);
}

Behaviour::~Behaviour()
{

}

void Behaviour::init(behaviours behaviourType)
{
	m_type = behaviourType;
	m_behaviourStarted.setToCurrentTime();
	m_duration = RNG::s_RNG->generateRandomNumber(m_behaviourDurations[m_type].m_min, m_behaviourDurations[m_type].m_max);
}

void Behaviour::update()
{
	m_behaviourTmp.setToCurrentTime();
	m_behaviourDiff = Time(m_behaviourTmp - m_behaviourStarted).getSeconds();

	if(m_behaviourDiff >= m_duration)
	{
		switch (m_type)
		{
		case ATTACK:
			changeBehaviour(RETREAT);
			break;
		case RETREAT:
			changeBehaviour(DISPURSE);
			break;
		case DISPURSE:
		{
			int tmp = RNG::s_RNG->generateRandomNumber(0, 3);
			tmp <= 2 ? changeBehaviour(FORMUP) : changeBehaviour(IDLE);
		}
			break;
		case FORMUP:
			changeBehaviour(ATTACK);
			break;
		case IDLE:
			changeBehaviour(FORMUP);
			break;
		}
	}
}

void Behaviour::changeBehaviour(behaviours newBehaviourType)
{
	init(newBehaviourType);
}


behaviours Behaviour::getType() const
{
	return m_type;
}

float Behaviour::getDuration() const
{
	return m_duration;
}