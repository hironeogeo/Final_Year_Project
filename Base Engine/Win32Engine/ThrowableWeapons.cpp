#include "ThrowableWeapons.h"

#include "Soldier.h"

ThrowableWeapons::ThrowableWeapons()
	:Ranged(2.5f, 3.0f, 2), Stationary(2.5f, 3.0f, 2.0f, 1.0f), Weapon(2.5f, 2, nullptr)
{
	
}

ThrowableWeapons::~ThrowableWeapons()
{
	
}

void ThrowableWeapons::attack()
{
	// how do we want this to work?

	// are they either a ranged or a stationary weapon - 1 or the other

	// or can it be both and the weapon will randomly be ranged - whilst the user has enough ammo - 
	// and when not the user doesnt have ammo it is fully stationary
	
	// variable to work out if ranged or stationary  

	int statOrRnged;

	if (m_shotsFired <= Weapon::m_owner->getMaxAmmo()) // if has ammo to fire
	{
		// we need to decide if we want to be stat or ranged
		statOrRnged = RNG::s_RNG->generateRandomNumber(0, 1);
	}
	else
		statOrRnged = 1;

	switch(statOrRnged)
	{
	case 0:// fire as ranged weapon
		{
			Ranged::attack();
		}	
	break;

	case 1: //fire as stationary weapon
		{
			Stationary::attack();
		}
	break;
	}

	


	
	// = 
}