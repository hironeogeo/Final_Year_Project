#include "General.h"

General::General(const XMFLOAT3& pos, Weapon* weapon, Ammunition* ammo, LPWSTR fileName, Team* team)
	:Soldier(fileName, weapon, ammo, pos, 1.0f, 1.0f, 5.0f, 5.0f, 4.0f, Behaviour(IDLE), team, 10, 10)
{

}

General::~General()
{

}

void General::behave()
{
	switch (m_currentBehaviour.getType())
	{
	case ATTACK:
	{
		if (m_weapon->getHasRefreshed())
		{
			m_weapon->attack();
		}

		if (m_team->getDirectionFacing() == LEFT)
		{
			XMFLOAT3 tmp = m_sprite->getPosition();
			tmp.x -= 0.025f + (m_speed + m_movement);
			m_sprite->changePosition(tmp);
		}
		else
		{
			XMFLOAT3 tmp = m_sprite->getPosition();
			tmp.x += 0.025f + (m_speed + m_movement);
			m_sprite->changePosition(tmp);
		}
	}

	//m_currentBehaviour = FORMUP;
	break;
	case RETREAT:
	{
		// move away from the enemy
		if (m_team->getDirectionFacing() == LEFT)
		{
			XMFLOAT3 tmp = m_sprite->getPosition();
			tmp.x += 0.05f + (m_speed + m_movement);
			m_sprite->changePosition(tmp);
		}
		else
		{
			XMFLOAT3 tmp = m_sprite->getPosition();
			tmp.x -= 0.05f + (m_speed + m_movement);
			m_sprite->changePosition(tmp);
		}
	}
	break;
	
	case IDLE:// do nothing

		if (m_team->getDirectionFacing() == LEFT)
		{
			XMFLOAT3 tmp = m_sprite->getPosition();
			tmp.y -= 0.05f + (m_speed + m_movement);
			m_sprite->changePosition(tmp);
		}
		else
		{
			XMFLOAT3 tmp = m_sprite->getPosition();
			tmp.y -= 0.05f + (m_speed + m_movement);
			m_sprite->changePosition(tmp);
		}
		break;
	}
}

void General::update()
{
	if (m_isAlive)
	{
		behave();
		processCollisions();

		Soldier::update();
	}
}

void General::render()
{

}