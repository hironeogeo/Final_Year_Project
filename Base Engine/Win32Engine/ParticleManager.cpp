#include "ParticleManager.h"

ParticleManager* ParticleManager::s_ParticleManager = NULL;

ParticleManager::ParticleManager()
{
	s_ParticleManager = this;
}

ParticleManager::~ParticleManager()
{

	for (auto& param : m_particles)
	{
		if (param)
		{
			param->update();
			try
			{
				if (!param->getIsAlive())
				{
					delete param;
					param = nullptr;
				}
			}
			catch (...)
			{

			}
		}
	}
}

void ParticleManager::updateParticles()
{
	for(auto& param : m_particles)
	{
		if (param)
		{
			param->update();
			if (!param->getIsAlive())
			{
				delete param;
				param = nullptr;
			}
		}
	}
}

void ParticleManager::addParticle(XMFLOAT3 pos)
{
	m_particles.push_back(new Particle(pos, 3));
}