#pragma once

class PowerUp
{
private: 
	float duration;
	int uses;

public:
	PowerUp();
	PowerUp(float duration, int uses);
	~PowerUp();
};
