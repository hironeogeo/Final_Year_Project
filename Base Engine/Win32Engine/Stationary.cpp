#include "Stationary.h"
#include "Soldier.h"

Stationary::Stationary(const float& damage, const float& speed, const int& refrershTime, const float& attackRadius)
	:Weapon(damage, refrershTime, nullptr)
{
	m_attackDuration = speed;
	m_attackRadius = attackRadius;
	m_isAttackPos = false;
}

Stationary::~Stationary()
{
	
}

void Stationary::attack()
{
	//float damage = m_damage * m_owner->getSoldierAmmo()->getDamage();
	//float speed = m_launchFactor * m_owner->getSoldierAmmo()->getSpeed();
	int direction = m_owner->getSoldierTeam()->getDirectionFacing();
	m_weaponPosition = m_owner->getSoldierSprite()->getPosition();
	
	if(direction == 0) // left
		m_weaponPosition.x += m_attackRadius;
	else
		m_weaponPosition.x -= m_attackRadius;

	m_isAttackPos = true;

	m_lastUsed.setToCurrentTime();
}

void Stationary::update()
{
	Weapon::update();

	if(m_isAttackPos)
	{
		m_tmp.setToCurrentTime();
		if(Time(m_tmp - m_lastUsed).convertToSeconds() >= m_attackDuration)
		{
			m_weaponPosition = m_owner->getSoldierSprite()->getPosition();
		}
	}


}