#pragma once
#include "string"
#include <vector>
class Sprite
{
private:
	std::string m_spriteName;
	std::vector<float> m_pos;
	int m_frame;

public: 
	Sprite();
	~Sprite();

};
