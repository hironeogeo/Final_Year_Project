#pragma once
#include <random>
#include <chrono>

class RNG
{
private:
	std::mt19937 m_random_number_engine;// (time(0));

public:
	static RNG* s_RNG;
	RNG();
	~RNG();

	int generateRandomNumber(const int& min, const int& max);
};