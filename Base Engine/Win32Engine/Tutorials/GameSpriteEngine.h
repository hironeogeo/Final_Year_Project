#pragma once

#include"Dx11DemoBase.h"
#include"GameSprite.h"


class GameSpriteDemo : public Dx11DemoBase
{

private:
	ID3D11VertexShader* solidColorVS_;
	ID3D11PixelShader* solidColorPS_;

	ID3D11InputLayout* inputLayout_;
	ID3D11Buffer* vertexBuffer_;

	ID3D11ShaderResourceView* colorMap_;
	ID3D11SamplerState* colorMapSampler_;
	ID3D11BlendState* alphaBlendState_; // used to render our textured sprites using alpha transparency - images must be 32 bit with alpha enabled

	GameSprite sprites_[2];
	ID3D11Buffer* mvpCB_; // model view projection constant buffer - used to send the mvp matrix to the buffer
	XMMATRIX vpMatrix_; // view projection matrix - no camera currently so can populate this once and then re use it

public:
	GameSpriteDemo();
	virtual ~GameSpriteDemo();

	bool loadContent();
	void unloadContent();

	void update(float dt);
	void render();


};
