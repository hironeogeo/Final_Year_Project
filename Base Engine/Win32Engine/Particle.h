#pragma once

#include "Sprite.h"
#include "Time.h"
#include "SpriteManager.h"

class Particle
{
private:
	Sprite* m_particleSprite;
	int m_duration;
	Time m_created;
	Time m_tmp;
	bool m_isAlive;
	int m_spriteID;

public:
	Particle();
	Particle(XMFLOAT3 pos, const int& duration);
	Particle(LPWSTR spritePath);
	Particle(LPWSTR spritePath, XMFLOAT3 pos);
	~Particle();

	void update();

	Sprite* getParticleSprite();
	int getParticleDuration() const;
	Time* getTimeCreated();
	bool getIsAlive() const;

};
