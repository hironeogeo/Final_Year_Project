﻿#pragma region Unassigned Refereneces

#pragma endregion 

#pragma region Game Engine Architecture

#pragma region Game Engine Architecture 2009

// Game Engine Architecture
// Jason gregory 2009
// Gregory, J. (2009).Game engine architecture.Natick, Mass.: Peters.

// (Gregory, 2009)
// "monolithic class hierarchies" chapter 14 (runtime gameplay foundation system) page 718 => 719

// " when I got my first game console in 1979 the term "game engine did not exist" 

// (now game engines) "have become fully featured reusable software development kits that can be 
// licensed and used to build almost any game imaginable" page 3

// "while game engines vary widely in details of their architecture and implementation, recognizable coarse-grained
// patterns are emergine aross both publically licensed game engines and their proprietarty in-house counterparts". p.3
//(Gorlen, Orlow & Plexico, 1991)
// "virtually all game engines contain a familiar set of core components"

// ^^^ Game engine architecture page 3
#pragma endregion

#pragma region Game engines : tools for landscape visualization and planning 2002
//Herwig, A., &Paar, P. (2002).Game engines : tools for landscape visualization and planning.Trends in GIS and Virtualization in Environmental Planning and Design, 161, 172.
//Chicago

// "A “game engine” contains the core algorithms controlling a game."

#pragma endregion

#pragma region Visualisation using game engines 2004
// "Fritsch, D., & Kada, M. (2004). Visualisation using game engines. Archiwum ISPRS, 35, B5.

// " Game engines are powerful software packages"

// "Today, 3D computer games are highly complex systems that
//consist of a universal game engine and the specific game
// elements like the game rules and game data(e.g.geometry,
//textures and sound files)."

// Game engines incorporates all sorts of elements that are vital to a game like physics, graphical
// user interface (GUI), artificial intelligence, network functionality, sound and event engine page 2

// (a game engine) "represents the basic framework independent of the game."

#pragma endregion

#pragma region Affective game engines: Motivation and requirements 2009

// Hudlicka, E. (2009). Affective game engines: Motivation and requirements. Proceedings of the 4th International Conference on Foundations of Digital Games, 299-306.
//game engines : development tools that facilitate the creation of
//games by providing realistic graphics and real - time simulation
//environments.

//"game engines : development tools that facilitate the creation of
// games by providing realistic graphics and real - time simulation
// environments."

#pragma endregion 

#pragma endregion 

#pragma region Fundamentals of OOP

#pragma region The Object-Oriented Thought Process 2004
// The Object-Oriented Thought Process
// Matt Weisfeld
// Book
// Fundamentals of OOD and OOP

// Weisfeld, M. (2004). Object-Oriented Thought Process, The, Second Edition. Sams.

// "OO programming is that the data and the operations that manipulate 
//  the data (the code) are both encapsulated in the object" page 9

// "the data stored within an object represents it's state" page 9

// "classes can be thought of as templates" page 14

// "one of the most powerful attributes of OO programming is code reuse" page 21

// "OO programming allows you to define relationships between classes that facilitate not only code reuse but better overall design" page 21

// "Polymorphism is one of the most powerful advantages of object oriented programming" page 24

// "multimple inheritance allows a class to inherit from more than one class" page 58

// "multiple inheritance can solve some problems quite elegantly haowever can siginificantly increase the complexity of the system" page 59

// "public interface methods" page 69

// "private implementation methods" page 69

// "design with extensibility in mind" ... "you do not want to design a class that contains behaviour that prevents it from being extended" page 76

// "design with maintainability in mind" .. "Designing useful and concise classes prompts a high level of maintainability. 
// Just as you design a class with extensibility in mind, you should also design with future maintenance in mind" page 80 

// "the waterfall model advocates strict boundries between the various phases" page 86

// "in practice the waterfall midel has found to be unrealistic" page 86

// "When OO technologies first entered the mainstream, inheritance was all the rage.
//  the fact that you could deesign a class once and then inherit functionality from 
// it was concidered the foremost advantage of using OO. Reuse was the name of the 
// game and inheritance was the ultimate expression of reuse" page 111

// "The fact that inheritance is often misused and overused is more a result of lack of 
// understanding of what inheritance is all about than a fundamental flaw in using 
// inheritance as a design strategy" page 112

// "Encapsulation is so fundamental to OO that is is one of the OO design's cardinal rules" page 120

// "However in one way inheritance actaully breaks encapsualtion" page 120 => talk about how inheritance 
// increases the coupling in the hierarchy, is this a potential trade off for (often) natural changes 
// as well as the gains made in code reusability.

// "A detailed example of Polymorphism" page 122

#pragma endregion

// The evolution of C++ language design in the marketpalce of ideas
// book, Jim Waldo 1993
// the case against multiple inheritance by T.A Cargill
// the case for multiple inheritance Jim Waldo

#pragma region Data Abstraction and Object-Oriented Programming in C++
// Data Abstraction and Object-Oriented Programming in C++
// Kieth E. Gorlen, Sanford M. Orlow, Perry S. Plexico 1991
// Book 
// Fundamentals of OOP and object oriented design

// Gorlen, K., Orlow, S., & Plexico, P. (1991). Data abstraction and object-oriented programming in C++. Stuttgart: Teubner [etc.].
// (Gorlen, Orlow & Plexico, 1991)

// "programmers have long recognized the value of 
// organizing related data items in program 
// constructs like Pascal RECORDS or C structs, 
// and then treating the resulting data structures as units." similar definition to the ADT in Sengupta and Korobkin book. page 1

// "Data abstraction extends this organization to encompass a set of operations that can be performed on a 
// particular instance of the structure" page 1

// "Usually the data elemts and the implementation of the operations that can be performed on them
// are held privately or ENCAPSUALTED to prevent unwanted alteration" justified use of the encapsualtion OO principle here page 1

// "We need not understand or even be aware of the inner workings, just as we do not 
// need to know eactly how a compiler treats a fundamental data type" similar explanation providing 
// validity to the ADT in Sengupta and Korobkin. book page 1

// "C++, for the first time gives us the programming tools and resources to write efficient 
// object-oriented programs for the genera-purpose computing environment" why C++ here you go! page 2

// "object-oriented programming deals with the manipulation of objects" page 2

// "Much of the value of object oriented progamming results from inheritance" page 2

// "One of the benefits of object oriented programming: code reusability" page 2

// (paraphrased) "introduction to oop - encapsulation, inheritance and dynamic binding" 
// ... derived classes .. virtual member functions page 101 => 102

// (paraphrased) "introduction to multiple inheritance and modular programming" page 293 => 294
#pragma endregion

#pragma region C++ Object-Oriented Data Structures 1994
// C++ Object-Oriented Data Structures
// Saumyendra Sengupta, Carl Phillip Korobkin 1994
// Book

//(Sengupta & Korobkin, 1994)
//Sengupta, S., &Korobkin, C. (1994).C++ Object - Oriented Data Structures.New York : Springer - Verlag.

// Fundamentals of OOP && Efficient us of in built Data Structures (ie lists, trees, arrays)
// "abstract data structures (ADT), An ADT is a collection of data and a set of allowed operations (actions) that are 
// used to define and manipulate the data" page 2

// "object-oriented design (OOD) decomposes most real-world applications as a collection of 
// cooperating objects, it follows that an ADT (and therefore an object oriented data structure) is an intergeral feature of OOD" page 6

// " the basic features of OOD include the following: " page 7

// " the purpose of good object orienred design is to present the user wih a 
// rich set of extensive, interchangable and reusable components upon which a software system is built" page 8

// "in an object oriented design, the user is more concerned with what the object does and not how it does it" page 8
// (paraphrased) "regardless of the design the implementor or user is usually concerned with the performance of the software system" page 9

// " in an object oriented design, one may be concerned with not only only what 
// an object does but how well it does it" => leading nicely onto performance!!! page 9

// " Steps For Deriving An Object-Oriented Design" page 11 => 12

// " Implementing an OOP in C++ ... single and multiple inheritance ... 
// virtual and overloaded functions supporting polymorphism ... dynamic memopry management" page 12 => 13

// " data abstraction and encapsulation are implemented using the C++ class construct" here we 
// have our differentiator between OO and DO (the data can be kept outside the class!!) page 13

// "Big Oh Notation" (again a link to performance which is what we need) page 17
#pragma endregion

// The Latency Elephant.
// Tony Albrecht 2009
// web article
// basic fundamentals && OO vs DO
// http://seven-degrees-of-freedom.blogspot.co.uk/2009/10/latency-elephant.html

#pragma endregion

#pragma region Fundamentals of DOD

//Data - oriented design considers data first : how it is laid out and how it is read and processed in the program" (Lengyel, 2011)
//"Then the code is something written to transform the input data into the output data but it's self is not the focus." (Lengyel, 2011)
//“Data - oriented design shifts the perspective of programming from objects to the data itself : The type of the data, how it is laid out in memory, and how it will be read and processed in the game.”(Llopis, 2009)
//“The purpose of all programs and all parts of those programs, is to transform data from one form to another” mike action cppcon 2014
//“if you don’t understand the data you don’t understand the problem” mike action cppcon 2014
//“separate states that we can reason with” mike action cppcon 2014 = > don’t use last minute decisions with bools have separate functions for separate problems.Pre condition data so that we are only processing data that we actually want.We have done this with the ammunition collisions in that we are only processing alive objects as supposed to checking everyone during the processing.
//“organised data makes maintenance, debugging and concurrency much easier” mike action cppcon 2014


#pragma region Game engine gems 2 2011

// Lengyel, E. (2011).Game engine gems 2. Natick: A K Peters.

// "Common programming wisdom used to encorage delaying optimizations until later in the project, and then
//  optimizing only those parts that were obvious bottlenecks in the profiler. Tha approach worked well 
//  with glaring inefficiencies, like particularly slow algorithms or code that is called many times per 
//  frame." page 251

// "Data oriented design helps us address this problem by architecting the game wih memory accesses 
//  and parallelization from the beginning" page 251

// "general definition of a computer program is "something that transforms  input data into output data"
// page 253

// "object-oriented programming deals mostly with objects (which are sets of data and the code 
//  that works on the data)" page 253

// "Data-oriented design turns that around and considers data first: how it is laid out and how 
//  it is read and processed in the program" page 253

// "Then the code is something written to transform the input data into the output data but 
// it's self is not the focus." page 253

// "Data oriented design encorages optimizing for the common case of having multiple objects 
//  of the same type" page 254

// "data oriented deesign has three major performance benefits: cache utilization ... 
//  parallelization ... less code" page 254

// "benefits from a development perspective ... easier to test ... 
//  easier to understand" page 254 => 255

// "2 disadvantages ... isnt taught in comp scie curricular ... difficult to link 
//  with 3rd party api ... harder to see the big picture" page 255

#pragma endregion

#pragma region Data-Oriented Architecture: A Loosely-Coupled Real-Time SOA 2007
// Joshi, R. (2007). Data-Oriented Architecture: A Loosely-Coupled Real-Time SOA | Data Distribution Service (DDS) Community RTI Connext Users. [online] Community.rti.com. Available at: https://community.rti.com/archive/data-oriented-architecture-loosely-coupled-real-time-soa.

// " Our existing methodologies and training for system software design, rooted in principles
// of object - oriented design, that worked superbly for small scale systems begin to break 
// down as we discover operational limits which requires frequent and unintended redesigns 
// in programs year over year."

// "Fundamentally, object-oriented thinking leads us to think in terms of tightly - coupled 
//  interactions that include strong state assumptions."

// "the principle of "data-oriented" design: expose the data and hide the code."

// "When we examine the practice of system design, we can identify two lines
//  primary schools of thought, distinguished along the lines of “tight - coupling” vs.
//  “loose - coupling” of components.These have been applied to the design of both
//  local and distributed processing systems."

// "Changes in one component’s interface typically 
// have a significant impact on the components that interact with it."

// "object - oriented programming paradigm is so widely popular and
// successful for local processing"

// "It is complementary to “object-oriented programming”(OOP), often practiced using 
// Java or C++.DOP is like OOP in the sense of being a conceptual framework that is 
// above any specific programming language or implementation technologies."

// "We should note that it always possible to tighten up loosely-coupled software;
//  however it is not possible to loosen up tightly coupled software"

// "It is important to note that a design paradigm by itself does not result in 
//  well designed systems"

// "design is fundamentally a human activity; paradigms and tools can
//  only facilitate the process." use this as justifcation for the modified build system type

#pragma endregion



// Practical Example of Data Oriented Design 
// knight666.com by admin
// Website / tutorial june 2011
// theory and uses of DOD
// http://knight666.com/blog/tutorial-a-practical-example-of-data-oriented-design/

// Introduction to Data - Oriented Design
// wwww.dice.se 2014
// PDF - stored local
// basic fundamentals with examples of DOD
// http://www.dice.se/wp-content/uploads/2014/12/Introduction_to_Data-Oriented_Design.pdf

//DICE. (2014).Introduction to Data - Oriented Design.Retrieved from http ://www.dice.se/wp-content/uploads/2014/12/Introduction_to_Data-Oriented_Design.pdf

// The Latency Elephant.
// Tony Albrecht 2009
// web article
// basic fundamentals && OO vs DO
// http://seven-degrees-of-freedom.blogspot.co.uk/2009/10/latency-elephant.html

// Acton, M. (2014). Data-Orientated Design and C++. [PowerPoint slides]. Retrieved from http://sched.co/1sjWVbq.

#pragma endregion

#pragma region OOD vs DOD

#pragma region A survey of software design techniques 1986
// Yau, S., &Tsai, J. (1986).A survey of software design techniques.Software Engineering, IEEE Transactions on, SE - 12(6), 713 - 721.

// "The architectural design stage, also called the preliminary or general design stage, is 
//  responsible for decomposing requirement specifications to form a system structure. It 
//  emphasizes the module - level system representations which can be evaluated, refined, 
//  or modified in the early software development process.

#pragma endregion

// Data-Oriented Design Now And In The Future
// noel_llopis. 2011
// web article
// DOD + vs OOD

// Data-Oriented Design (Or Why You Might Be Shooting Yourself in The Foot With OOP)
// noel_llopis. 2009
// web article + Septemper 2009 issue of Game Developer (magazine?)
// fundamentals of DOD and vs OOD
// http://gamesfromwithin.com/data-oriented-design

// Llopis, N. (2009). Data-Oriented Design (Or Why You Might Be Shooting Yourself in The Foot With OOP) – Games from Within. Gamesfromwithin.com. Retrieved from http://gamesfromwithin.com/data-oriented-design

// Llopis, N. (2011). Data-Oriented Design Now And In The Future – Games from Within. Gamesfromwithin.com. Retrieved from http://gamesfromwithin.com/data-oriented-design-now-and-in-the-future

// The Latency Elephant.
// Tony Albrecht 2009
// web article
// basic fundamentals && OO vs DO
// http://seven-degrees-of-freedom.blogspot.co.uk/2009/10/latency-elephant.html

//Albrecht, T. (2009).The Latency Elephant..Seven - degrees - of - freedom.blogspot.co.uk.Retrieved 15 January 2018, from http ://seven-degrees-of-freedom.blogspot.co.uk/2009/10/latency-elephant.html

#pragma endregion

#pragma region DOD and Memory Optimisation

// Memory Optimization
// Christer Ericson 2003
// GDC Conference - slides
// DO and memory optimisation techniques
// local slides - GDC03_Ericson_Memory_Optimization.ppt

#pragma region Game Engine Architecture
// Game Engine Architecture
// Jason gregory 2009

// "data that is located in small, contigious blocks of memory can be 
//  operated on much more efficiently by the CPU than if the same data 
//  were to be spread out accross a wide range of memory addresses" page 206

// "Accessign main system RAM is always a slow operation, often taking 
//  thousands of processor cycles to complete" page 220

// "a cache is a special type of memory that can be read from and written to 
//  much faster by the CPU much more quickly than main RAM" page 220

// "the best way to avoid data cache misses is to organise your data in 
// contiguous blocks that are as small as possible and then access them sequentially" page 220

// "if the algorithm performas a loop over the elements in the container and visits each 
//  element once we say the algorithm is O(n) page 227

#pragma endregion

#pragma region Real Time Collision Detection

// Real Time Collision Detection
// Christer Ericson 2004
// Book - page 511 ->
// data and memory optimisations
// borrowed from jacob

//Ericson, C. (2004).Real - time collision detection.Amsterdam: Elsevier.

// "Over the last several generations of CPUs, processor speeds have increased faster than memory access times have decreased" chapter 13 optimization page 511

// "To help reduce this problem, CPU manufacturers are incorporating one or more levels of fast cache memory to sit between 
// main memory and the CPU. These effectively implement a memory hierarchy in which higher but slower levels are only 
// accessed if data cannot be found in the faster but slower levels" chapter 13 optimization page 511

// (paraphrasing) "cache memory helps this problem but it is still important to performance-sensitive 
// code be written specifically to take advantage of this memroy hierarchy" chapter 13 optimization page 511

// "It is up to programmers to take advantage of it by explicitly implementing memory optimization" chapter 13 optimization page 511

// "Do not try to intuit where optimization is needed; always use a code profiler to guide the optimization. " page 512
// => can be used to justify visual studio as the IDE of choice but also to justify the optimisation areas we are targeting

// "Only when bottlenecks have been identified and appropriate algorithms and data structures have been selected should fine-tuning be performed" page 512

// "It also makes sense to measure the efficiency of an optimization through profiling" page 512

// "To work arond the latencies in accessing main memiry, small high-speed memories, caches executing at virtually the same 
//  speed as the cpu are introduced" page 513

// "The utilizatin of  a data cache is also improved by reducing the size and increasing both the spatial and temporal locality of the data processed" page 517

// "Data locality can be improved by redesigning algorithms and data structutes to allow data to be accessed in a more predictable, often 
//  linear or partially linear, fashion page 518

// "Improving data locality may also improve a process often referred to as as blocking: breaking large data into 
//  smaller chunks that fit into cache" page 518 This is what we have done!!!!

// "An example would be the seperation of collision detection data from rendering data to avoid having rgb colours, 
//  texture coordinates and other data irrelevant to collision detection reduce data locality by being interleaved 
//  with verticies and other relevant data. page 518

// how structures and classes can be optimized

// "Decrease the overall size d rthet structre. By not using more bits than necessary to represent member fields, 
//  more data fit into cache line and the number of memory fetches required to access structure fields is reduced" page 518

// "reorder fields within the structure. Fields within a structure are usually groouped conceptually, but should reallyy 
//  be grouped on access patterns so that those typically accessed together are stored together page 518

// "split the structure into hot and cold parts. Often a few structure fields are being accessed 
// more frequently than other fields. By moving the infrequently accessed fields into a seperate structure, 
// cache coherency increases among the frequently accessed ones, in particular when the structure is part 
// of an array or some similar conglomerate data structure" page 518

// "the fields of position, velocity and acceleration are frequently accessed simulteneously and could benefit 
// from from being stored together" page 520

// "The concept of hot/cold splitting on structures entails splitting the structure into two different parts: 
//  one containing the frequently accessed fields (hot part) and the other infrequnelty accessed fields (the cold part).
//  these pieces are alloated and then stored seperately, and a link pointer to the corresponding cold part is embedded 
//  in each hot part" => this is how we have built our collision component but we can also talk about why our position 
//  component is different ie cold to hot. page 520

// cache obliviousness

// "the idea behind cache-oblivious algorithms and data structures is to optimize for an ideal cache model with a
//  cache line size L and a memory size Z, where both L and Z are unknown" page 531

// software caching  & data linearization

// "The rearrangement of concurrently accessed but dispersed data into contiguous blocks to 
//  improve performance by increasing the spacial locality" page 531

// "by performing on demand linearization and caching the result, it is possible to bring together 
//  data at run time that cannot practically be stored together otherwise" page 513 
//  can use this as back up for recreating the collidable ammo every frame as well as the seperation of pos and move components

#pragma endregion

// Acton, M. (2014). Data-Orientated Design and C++. [PowerPoint slides]. Retrieved from http://sched.co/1sjWVbq.

//“To achieve the best possible data layout, it’s helpful to break down each object into the different components, and group components of the same type together in memory, regardless of what object they came from.This organization results in large blocks of homogeneous data, which allow us to process the data sequentially”(Llopis, 2009)
//“In order to extract a high level of performance, a programmer *must* consider the data over the processing of that data”(Albrect, 2009)
//“spatial and temporal locality of data is a necessary goal here”(Albrect, 2009)
//“Solve for the most common case first not the most generic” mike action cppcon 2014
//“premature optimization is the most used quote of all time” mike action cppcon 2014 // what he means here is this methodology of building something before you have tested it as a full intergrated part of the complete system, such as you might use something like an analyser. It is often done too early in the software development lifecycle and involves programmers optimising something because they assume they will get a performance gain as supposed to having the complete picture.


#pragma endregion 

#pragma region game context research

// GameSpot. (2006).Medieval 2: Total War Review.GameSpot.Retrieved from https ://www.gamespot.com/reviews/medieval-2-total-war-review/1900-6161703/
// (GameSpot, 2006)

// Gamespot. (2009).Empire: Total War Review.Gamespot.Retrieved from https ://www.gamespot.com/reviews/empire-total-war-review/1900-6205809/
// (GameSpot, 2006)

//(Total War Wiki, 2006)
// Total War Wiki. (2006). Units in Medieval II: Total War. Wiki.totalwar.com. Retrieved 10 January 2018, from http://wiki.totalwar.com/w/Units_in_Medieval_II:_Total_War

#pragma endregion 

#pragma region Development Models

#pragma region Comparative study on software development methodologies 2014
//Mihai Liviu Despa. (2014).Comparative study on software development methodologies.Database Systems Journal, (3), 37 - 56.

// "Software development methodologies follow two major philosophies : heavyweight and lightweight. "

/*Heavyweight methodologies
are derived from the waterfall model and
emphasizes detailed planning, exhaustive
specifications and detailed application
design */

// "Heavyweight methodologies are suitable for projects where requirements are unlikely to change and 
//  the software complexity allows for detailed planning. "

// "Lightweight methodologies are suitable for projects were specifications are unclear or are
//	likely to change due to project internal or external factors."

// "Lightweight methodologies are based on an incremental approach were software is delivered in
// multiple consecutive iterations, all of them being functional versions of the application. "

// "Lightweight methodologies provide great flexibility and can easily adapt to change."

// "When choosing a software development methodology project owner profile, developer’s 
// technical expertise, project complexity, budget and deadlines must be taken into account."

// "Often no methodology will fit perfectly the profile of a specific project."

#pragma endregion


// Geambasu, Cristina Venera, Jianu, Iulia, Jianu, Ionel, &Gavrila, Alexandru. 
// (2011).INFLUENCE FACTORS FOR THE CHOICE OF A SOFTWARE DEVELOPMENT METHODOLOGY.Accounting 
// and Management Information Systems, 10(4), 479 - 494.

#pragma region Choice of Software Development Methodologies : Do Organizational, Project, and Team Characteristics Matter 2016
//Vijayasarathy, L. and Butler, C. (2016).Choice of Software Development Methodologies : Do Organizational, Project, and Team Characteristics Matter ? .IEEE Software, 33(5), pp.86 - 94.
// (Vijayasarathy and Butler, 2016)

// "SOFTWARE DEVELOPMENT methodologies provide a framework for planning, executing, and managing
// the process of developing software systems."

// "Researchers have argued that there’s no silver bullet or one size fits all solution to software development"

// "55.5 percent of the companies that used traditional approaches were high-revenue companies (>$1B)."

// " Of the companies that used traditional approaches, 55.5 percent had more than 10,000 employees,
// and 77.7 percent had more than 1, 000 employees.In contrast, of the companies that used hybrid,
// agile, or iterative approaches, 45.8, 34.1, and 30.0 percent, respectively, had more than 1, 000 
// employees. Of the companies that used iterative, agile, and hybrid approaches, 70.0, 48.8, and
// 37.3 percent, respectively, had 1 to 250 employees.In comparison, only 16.7 percent of companies
// that used traditional methodologies had 1 to 250 employees. Most companies(60 percent) that used 
// iterative methodologies had from 1 to 50 employees."

// "In summary, companies with high employee counts were the dominant group for traditional approaches,
// whereas companies with low employee counts were dominant for agile and iterative approaches."

// "For example, 51.2 percent of agile - approach projects had high criticality, compared to 88.2
// percent of the traditional - approach projects.This data suggests that organizations
// tend to use traditional approaches on critical projects."

// "The number of teams and team size had a signicant relationship with the development approach."

//  2Projects with one team were the most prevalent with agile and iterative approaches 
// (48.9 and 50.0 percent, respectively)." 

// "Projects with four or more teams were the most prevalent with traditional and hybrid approaches
//(55.6 and 40.7 percent, respectively). "

// "Projects with two or three teams were distributed fairly consistently, ranging from 30.0 percent
// for iterative approaches to 39.5 percent for agile approaches. "

// "The results differed somewhat for team size(see Figure 8b), which we classidied as small(≤10), medium
// (11 to 30), or large(>30)."

// "Small teams were the most prevalent with agile, iterative, and hybrid 
// approaches (69.8 percent, 80.0 percent, and 50.8 percent, respectively)."

// "Medium teams were the most prevalent with traditional approaches(61.1 percent)." 

// "Large teams were rare, ranging from 9.3 percent for agile approaches to 16.7 percent 
// for traditional approaches."

// "no particular methodology was used in more than one - third of the projects"

// (paraphrasing) use of agile methodologies have risen since 2003

#pragma endregion

#pragma region Comparative Study on Agile software development methodologies 2013
//Moniruzzaman, A., &Hossain, D. (2013).Comparative Study on Agile software development methodologies.Global Journal of Computer Science and Technology(c) Volume 13 Issue 7 Version I.

// "Agile software development methodologies have become increasingly popular 
//  in software development industry"

// "One of the main reasons for success with agile methods is that they are highly adaptive"

// " almost 70% of all software projects fail. "

// "the main difference between heavyweight and agile methodologies is the acceptance of change"

#pragma endregion

#pragma endregion

#pragma region Research Models

#pragma region about computing science research methodology 2011
// Nelson Amaral, J. (2011).About Computing Science Research Methodology.[online] Citeseerx.ist.psu.edu.Available at : http://citeseerx.ist.psu.edu/viewdoc/versions?doi=10.1.1.124.702
// (Nelson Amaral, 2011)

#pragma endregion

//Duda, R. O., Hart, P. E., & Stork, D. G. (1973). Pattern classification (pp. 526-528). Wiley, New York.
//Chicago	


#pragma region Inductive Approachs

#pragma region Inductive Approach (Inductive Reasoning) - Research Methodology 2011
// Dudovskiy, J. (2011). Inductive Approach (Inductive Reasoning) - Research Methodology. Research Methodology. Retrieved 9 January 2018, from https://research-methodology.net/research-methodology/research-approach/inductive-approach-2/
// (Dudovskiy, 2011)

// It is important to stress that inductive approach does not imply disregarding theories when 
// formulating research questions and objectives. This approach aims to generate meanings from 
// the data set collected in order to identify patterns and relationships to build a theory

#pragma endregion 

//Bernard, H.R. (2011) “Research Methods in Anthropology” 5th edition, AltaMira Press, p.7
//		“involves the search for pattern from observation and the development of explanations – theories – for those patterns through series of hypotheses”

//Goddard, W. & Melville, S. (2004) “Research Methodology : An Introduction” 2nd edition, Blackwell Publishing
//		Inductive approach, also known in inductive reasoning, starts with the observations and 
//		theories are proposed towards the end of the research process as a result of observations

#pragma region hallidayhannah 2016
// hallidayhannah. (2016).Quantitative, Qualitative, Inductive and Deductive Research.Slideshare.net.Retrieved 9 January 2018, from https ://www.slideshare.net/hallidayhannah/quantitative-qualitative-inductive-and-deductive-research?from_action=save
// (hallidayhannah, 2016)

// in an inductive approach to research, a researcher begins by collecting data that is relevant to his or her topic of interest

// once a considerable amount of data has been collected, the researcher will 
// then take a time out from data collection, stepping back to get a bird's eye vie of the data

// at this stage , the researcher looks for patterns in the data working to develop a theory that could explain those patterns

// Gather Data (specific level of focus) => Look for Patterns (Analsis) => Develop Theory (General level of focus) 

#pragma endregion

#pragma endregion

#pragma endregion

#pragma region Games industry

//Stuart, K. (2014, March 20).How to get into the games industry – an insiders' guide, London: Kings Place.

//https ://ukie.org.uk/research => games industy stats with lots of numbers

//http://store.steampowered.com/hwsurvey => tells us the most common hardware specs of pc's

#pragma endregion

#pragma region Rendering optimisations

// massively multiplayer game development 2 => book
// optimization techniques for rendering massive quantites of mesh deformed characters in real time => article

#pragma endregion
