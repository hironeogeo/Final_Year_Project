#include "Time.h"

Time::Time()
{
	time_t now = time(0);
	struct tm& t = *localtime(&now);
	hours_ = t.tm_hour;
	minutes_ = t.tm_min;
	seconds_ = t.tm_sec;
	t4hour_ = true;
	hours_ > 11 ? isPM_ = true : isPM_ = false;
	checkForPM();
}

Time::Time(const int& h, const int& m, const int& s)
{
	hours_ = h;
	minutes_ = m;
	seconds_ = s;
	isPM_ = false;
	t4hour_ = false;
	checkForPM();
	isValidTime();
}

Time::Time(const int& h, const int& m, const int& s, const bool& twenty4Hours)
{
	hours_ = h;
	minutes_ = m;
	seconds_ = s;
	t4hour_ = twenty4Hours;
	isPM_ = false;
	checkForPM();
	isValidTime();
}

Time::Time(const int& h, const int& m, const int& s, const bool& twenty4Hours, const bool& pm)
{
	hours_ = h;
	minutes_ = m;
	seconds_ = s;
	t4hour_ = twenty4Hours;
	isPM_ = pm;
	checkForPM();
	isValidTime();
}

Time::Time(const int& timeInSeconds)
{
	// does this function return a time in 12 hour format?
	// does it depend on the starting format?
	// test to see results!

	int tWH; // time withough hours 

	if (timeInSeconds >= 3600)
	{
		hours_ = (timeInSeconds / 3600);
		tWH = timeInSeconds - (hours_ * 3600);
		minutes_ = (tWH / 60);
		seconds_ = (tWH - (minutes_ * 60));
	}
	else
	{
		minutes_ = (timeInSeconds / 60);
		seconds_ = (timeInSeconds - (minutes_ * 60));
	}

}

Time::Time(Time &t)
{
	hours_ = t.getHours();
	minutes_ = t.getMinutes();
	seconds_ = t.getSeconds();
	t4hour_ = t.getTimeFormat();
	isPM_ = t.getPM();
}

Time::~Time()
{

}

void Time::isValidTime()
{
	if (hours_ > 23 || minutes_ > 59 || seconds_ > 59)
	{
		hours_ = 0;
		minutes_ = 0;
		seconds_ = 0;
		t4hour_ = false;
		isPM_ = false;
	}
}

const int Time::getHours()
{
	return hours_;
}

const int Time::getMinutes()
{
	return minutes_;
}

const int Time::getSeconds()
{
	return seconds_;
}

const bool Time::getTimeFormat()
{
	return t4hour_;
}

const bool Time::getPM()
{
	return isPM_;
}

void Time::setHours(const int& h)
{
	hours_ = h;
	checkForPM();
}

void Time::setMinutes(const int& m)
{
	minutes_ = m;
	checkForPM();
}

void Time::setSeconds(const int& s)
{
	seconds_ = s;
	checkForPM();
}

void Time::setTimeFormat(const bool& t4)
{
	t4hour_ = t4;
}

void Time::setIsPM(const bool& pm)
{
	isPM_ = pm;
}

void Time::setTime(const Time& rhs)
{
	hours_ = rhs.hours_;
	minutes_ = rhs.minutes_;
	seconds_ = rhs.seconds_;
	t4hour_ = rhs.t4hour_;
	isPM_ = rhs.isPM_;
}

void Time::toggleTimeFormat()
{
	if (t4hour_ == true)
	{
		t4hour_ = false;
	}
	else
	{
		t4hour_ = true;
	}
}

Time Time::getCurrentTime()
{
	time_t now = time(0);
	struct tm& t = *localtime(&now);
	int hours = t.tm_hour;
	int minutes = t.tm_min;
	int seconds = t.tm_sec;
	bool t4hour = true;
	bool ispm;
	hours_ > 11 ? ispm = true : ispm = false;
	
	return Time(hours, minutes, seconds,t4hour,ispm);
}

void Time::setToCurrentTime()
{
	time_t now = time(0);
	struct tm& t = *localtime(&now);
	hours_ = t.tm_hour;
	minutes_ = t.tm_min;
	seconds_ = t.tm_sec;

	if (t4hour_)
	{
		if (isPM_ && hours_ < 12)
		{
			isPM_ = false;
		}
	}
	else
	{
		if (hours_ > 12)
		{
			hours_ -= 12;
			isPM_ = true;
		}
		else
		{
			isPM_ = false;
		}
	}

}

int Time::convertToSeconds()
{
	int timeInSeconds = ((hours_ * 3600) + (minutes_ * 60) + seconds_);
	return timeInSeconds;
}

string Time::convertToString()
{
	string hours = to_string(hours_);
	string mins = to_string(minutes_);
	string secs = to_string(seconds_);

	if (t4hour_ == false)
	{
		string pm;
		if (isPM_)
		{
			pm = "PM";
		}
		else
		{
			pm = "AM";
		}
		setw(2);
		setfill("0");
			
		return string(hours + ":" + mins + ":" + secs + " " + pm);
	}
	else
	{
		return string(hours + ":" + mins + ":" + secs);
	}
}

Time Time::convertFromSeconds(const int& timeInSeconds)
{
	int tWH; // time withough hours 

	Time t;
	if (timeInSeconds >= 3600)
	{
		t.setHours(timeInSeconds / 3600);
		tWH = timeInSeconds - (t.getHours() * 3600);
		t.setMinutes(tWH / 60);
		t.setSeconds(tWH - (t.getMinutes() * 60));
	}
	else
	{
		t.setHours(0);
		t.setMinutes(timeInSeconds / 60);
		t.setSeconds(timeInSeconds - (t.getMinutes() * 60));
	}

	return t;
}

Time Time::convertTo24Hours()
{
	checkForPM();
	if (t4hour_ == false && isPM_)
	{
		t4hour_ = true;
		return(Time(hours_ + 12, minutes_, seconds_, t4hour_));
	}
}

Time Time::convertTo12Hour()
{
	checkForPM(); // check if its past 12
	if (t4hour_ == true && isPM_)
	{
		t4hour_ = false;
		// if it is past 12 and its a 12 hour clock - 12 hours from time
		return(Time(hours_ - 12, minutes_, seconds_, t4hour_));
	}
}

// This function is designed to return the midpoint between the initiating time object and 1 thats passed in (RHS)
Time Time::findMidPoint(Time& rhs)
{
	// UNFINISHED FIDDLY WAY OF SOLVING THIS
	// first check that the rhs time is in front of the calling time
	// {
		// STILL TO BE IMPLEMENTED
	// }
	// BECAUSE THGE IN FRONT OF FUNCTIIONALITY HAS YET 
	// TO BE DEFINED WE ARE GOING TO ASSUME THAT THE 
	// VALUES BEING PASSED IN ARE VALID
	//Time lhs(this->getHours(),this->getMinutes(),this->getSeconds(),this->getTimeFormat(),this->getPM());
	//Time diff = rhs - lhs;
	//diff = diff * 0.5;
	//Time midpoint = lhs + diff;
	//return midpoint;

	int time1 = this->convertToSeconds();
	int time2 = rhs.convertToSeconds();
	int diff = time2 - time1;
	diff *= 0.5;
	time1 += diff;

	Time midpoint = midpoint.convertFromSeconds(time1);
	return midpoint;
}

Time Time::findMidPoint(Time& t1, Time& t2)
{
	// UNFINISHED FIDDLY WAY OF SOLVING THIS
	// first check that the rhs time is in front of the calling time
	// {
	// STILL TO BE IMPLEMENTED
	// }
	// BECAUSE THGE IN FRONT OF FUNCTIIONALITY HAS YET 
	// TO BE DEFINED WE ARE GOING TO ASSUME THAT THE 
	// VALUES BEING PASSED IN ARE VALID
	//Time lhs(this->getHours(),this->getMinutes(),this->getSeconds(),this->getTimeFormat(),this->getPM());
	//Time diff = rhs - lhs;
	//diff = diff * 0.5;
	//Time midpoint = lhs + diff;
	//return midpoint;

	int time1 = t1.convertToSeconds();
	int time2 = t2.convertToSeconds();
	int diff = time2 - time1;
	diff *= 0.5;
	time1 += diff;

	Time midpoint = midpoint.convertFromSeconds(time1);
	return midpoint;
}

int Time::findMidvalue(const int& value1, const int& value2)
{
	return (value1 - value2) / 2;
}

void Time::checkForPM()
{
	if (t4hour_)
	{
		if (isPM_ && hours_ < 12)
		{
			hours_ += 12;
		}

		if (isPM_ == false && hours_ > 12)
		{
			hours_ -= 12;
		}
	}
	else
	{
		// its a 12 hour clock

		if (hours_ > 12) // if 12 hour and hours more than 12
		{
			hours_ -= 12;
			isPM_ = true;
		}
		else
		{
			isPM_ = false;
		}
	}

	checkForDayEnd();
}

void Time::checkForDayEnd()
{
	if (hours_ >= 24)
	{
		hours_ -= 24;
	}
}

void Time::removeMinus(int& time)
{
	if (time < 0)
	{
		time = (time * -1);
	}
}

Time Time::operator+(const Time& rhs)
{
	// 2 ways of doing this: 

	// method 1
	// first way convert both instances of the time class to seconds, add the 2 ints together, convert the sum back into a time
	//int t1 = convertToSeconds();
	//int t2 = rhs.convertToSeconds();
	//t1 += t2;
	//return convertFromSeconds(t1);

	// method 2
	// second way add the hours, minutes and seconds together respectivly and take into account the max values of 23,59 and 59

	int totalSecs = this->getSeconds() + rhs.seconds_;
	int totalMins = this->getMinutes() + rhs.minutes_;
	int totalHours = this->getHours() + rhs.hours_;

	// nTime variables are those that will be used to make up t3
	int nSeconds = 0;
	int nMinutes = 0;
	int nHours = 0;

	nMinutes += toolBox.calculateTimeToAdd(totalSecs, 60, nSeconds);
	nHours += toolBox.calculateTimeToAdd(totalMins, 60, nMinutes);
	toolBox.calculateTimeToAdd(totalHours, 24, nHours);

	return Time(nHours, nMinutes, nSeconds, t4hour_, isPM_); // returns time format of the LHS parameter
}

void Time::operator=(const Time& rhs)
{
	hours_ = rhs.hours_;
	minutes_ = rhs.minutes_;
	seconds_ = rhs.seconds_;
	t4hour_ = rhs.t4hour_;
	isPM_ = rhs.isPM_;
}

void Time::operator=(const Time* rhs)
{
	hours_ = rhs->hours_;
	minutes_ = rhs->minutes_;
	seconds_ = rhs->seconds_;
	t4hour_ = rhs->t4hour_;
	isPM_ = rhs->isPM_;
}

bool Time::operator==(const Time& rhs)
{
	if (seconds_ != rhs.seconds_)
	{
		return false;
	}

	if (minutes_ != rhs.minutes_)
	{
		return false;
	}

	if (hours_ != rhs.hours_)
	{
		return false;
	}

	if (isPM_ != rhs.isPM_)
	{
		return false;
	}

	return true;
}

bool Time::operator!=(const Time& rhs)
{
	if (seconds_ == rhs.seconds_)
	{
		return false;
	}

	if (minutes_ == rhs.minutes_)
	{
		return false;
	}

	if (hours_ == rhs.hours_)
	{
		return false;
	}

	if (isPM_ == rhs.isPM_)
	{
		return false;
	}

	return true;
}

Time Time::operator-(const Time& rhs)
{
	int totalSecs = this->getSeconds() - rhs.seconds_;
	int totalMins = this->getMinutes() - rhs.minutes_;
	int totalHours = this->getHours() - rhs.hours_;

	// nTime variables are those that will be used to make up t3
	int nSeconds = 0;
	int nMinutes = 0;
	int nHours = 0;

	nMinutes -= toolBox.calculateTimeToMinus(totalSecs, -60, nSeconds);
	nHours -= toolBox.calculateTimeToMinus(totalMins, -60, nMinutes);
	toolBox.calculateTimeToMinus(totalHours, -24, nHours);
	
	return Time(nHours, nMinutes, nSeconds, t4hour_, isPM_); // returns time format of the LHS parameter

}

void Time::operator++()
{
	seconds_ += 1;

	if (seconds_ == 60)
	{
		seconds_ = 0;
		minutes_ += 1;
	}

	if (minutes_ == 60)
	{
		minutes_ = 0;
		hours_ += 1;
	}

	if (hours_ == 24)
	{
		hours_ = 0;
		minutes_ = 0;
		seconds_ = 0;
	}
}

void Time::operator--()
{
	seconds_ -= 1;

	if (seconds_ == 0)
	{
		seconds_ = 59;
		minutes_ -= 1;
	}

	if (minutes_ == 0)
	{
		minutes_ = 59;
		hours_ -= 1;
	}

	if (hours_ == 0)
	{
		hours_ = 23;
		minutes_ = 59;
		seconds_ = 59;
	}
}