#pragma once
#include <string>
#include <vector>
class Text
{
private:
	std::string m_text;
	std::vector<float> pos;
public:
	Text();
	~Text();
	Text(std::string);
	void changeText(std::string newText);
	void moveText(std::vector<float> newPos);