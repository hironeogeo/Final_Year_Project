#include "Soldier.h"
#include "SpriteManager.h"
#include "Team.h"
#include "Recruit.h"
#include "Sergent.h"
#include "General.h"


Soldier::Soldier(LPWSTR spriteBodyName, Weapon* weapon, Ammunition* ammo, const XMFLOAT3& pos, const float& movement,
				const float& speed, const float& defense, const float firepower, const int& leadership, Behaviour startingBehaviour, 
				Team* team, int maxAmmo, const int& health)
	: m_sprite(nullptr), m_weapon(weapon), m_ammo(ammo), m_maxAmmo(maxAmmo), m_currentBehaviour(startingBehaviour), m_team (team)
{
	m_sprite = SpriteManager::s_spriteManager->addNewSprite(spriteBodyName, m_spriteID, XMFLOAT3(pos.x, pos.y, 1.0f));
	m_weapon->setParent(this);
	if(m_ammo)
	{
		m_hasAmmo = true;
		m_ammo->setParent(this);
	}

	m_pos = pos;
	m_movement = movement;
	m_speed = speed;
	m_defense = defense;
	m_firepower = firepower;
	m_leadership = leadership;
	m_isAlive = true;
	m_health = health;
}


Soldier::~Soldier()
{
	//Sprites cleaned up in the Sprite manager class

	cleanUp();
}

void Soldier::behave()
{

}

void Soldier::update()
{
	//m_weapon->update(); handled elsewhere
	//behave();

	XMFLOAT3 tmp = m_sprite->getPosition();
	bool positionNeedsResetting = false;
	if (m_team->getDirectionFacing() == LEFT)
	{
		if (tmp.x <= m_team->getOtherTeam()->getMinArea().x)
			positionNeedsResetting = true;

		if (tmp.x >= m_team->getMaxArea().x)
			positionNeedsResetting = true;

		if (tmp.y >= m_team->getMaxArea().y)
			positionNeedsResetting = true;

		if (tmp.y <= m_team->getMinArea().y)
			positionNeedsResetting = true;

	}
	else // facing right
	{
		if (tmp.x >= m_team->getOtherTeam()->getMaxArea().x)
			positionNeedsResetting = true;

		if (tmp.x <= m_team->getMinArea().x)
			positionNeedsResetting = true;

		if (tmp.y >= m_team->getMaxArea().y)
			positionNeedsResetting = true;

		if (tmp.y <= m_team->getMinArea().y)
			positionNeedsResetting = true;
	}

	if (positionNeedsResetting)
	{
		XMFLOAT3 resetPos;
		resetPos.x = (m_team->getMaxArea().x + m_team->getMinArea().x) * 0.5f ;
		resetPos.y = m_team->getMaxArea().y * 0.5f;
		resetPos.z = 1.0f;
		m_sprite->changePosition(resetPos);
	}

}

void Soldier::render()
{

}

void Soldier::cleanUp()
{
	if (m_weapon)
		delete m_weapon;

	if (m_ammo)
		delete m_ammo;

	m_weapon = nullptr;
	m_ammo = nullptr;
}

XMFLOAT3 Soldier::getSoldierPosition()
{
	return m_pos;
}

Weapon* Soldier::getSoldierWeapon()
{
	return m_weapon;
}

Behaviour* Soldier::getSoldierBehaviour()
{
	return &m_currentBehaviour;
}

Ammunition* Soldier::getSoldierAmmo()
{
	return m_ammo;
}

Sprite* Soldier::getSoldierSprite()
{
	return m_sprite;
}

Team* Soldier::getSoldierTeam()
{
	return m_team;
}

int Soldier::getMaxAmmo() const
{
	return m_maxAmmo;
}

void Soldier::processCollisions()
{
	// process own team soldiers
	std::vector<CollisionToRessolve> collisionsToResolve;

	XMVECTOR a, b, calc, dotRes;
	XMFLOAT3 res;
	int ans;

	std::vector<Recruit*> collidableRecruits;
	std::vector<Sergent*> collidableSergents;
	std::vector<General*> collidableGenerals;

	collidableRecruits = m_team->getOtherTeam()->getRecruits();
	collidableSergents = m_team->getOtherTeam()->getSergents();
	collidableGenerals = m_team->getOtherTeam()->getGenerals();
	
	// is the loop 1 faster that 2? after profiling it was found that the != operator in 
	// the rnage based for loop was very expensive so i wonder if the below could be a good upgrade?
	// could potentially show iteration as well? - answer = no after analysing it :(
	//size_t contLen = collidableRecruits.size();
	//for(int i = 0; i <= contLen; ++i) // loop 1
	//{
	//	if(collidableRecruits[i])
	//	{
	//		b = XMLoadFloat3(&collidableRecruits[i]->getSoldierSprite()->getPosition());
	//		a = XMLoadFloat3(&m_sprite->getPosition());
	//		calc = b - a;
	//		XMStoreFloat3(&res, XMVector3Dot(calc, calc));
	//		if (res.x <= 1000 && res.x > 0)
	//		{
	//			collisionsToResolve.push_back(CollisionToRessolve(res.x, XMVector3Normalize(a - b)));
	//		}
	//	}
	//}

	for (const auto& param : collidableRecruits) // loop 2
	{
		if (param)
		{
			b = XMLoadFloat3(&param->getSoldierSprite()->getPosition());
			a = XMLoadFloat3(&m_sprite->getPosition());

			calc = b - a;
			//dotRes = calc * calc;
			//XMStoreFloat3(&res, dotRes);
			//ans = res.x + res.y + res.z;

			XMStoreFloat3(&res, XMVector3Dot(calc, calc));

			//	XMVECTOR temp = XMVector3Dot(calc, calc);

			if (res.x <= 1000 && res.x > 0)
			{
				collisionsToResolve.push_back(CollisionToRessolve(res.x, XMVector3Normalize(a - b)));
			}
		}
	}

	for (const auto& param : collidableSergents)
	{
		if (param)
		{
			b = XMLoadFloat3(&param->getSoldierSprite()->getPosition());
			a = XMLoadFloat3(&m_sprite->getPosition());

			calc = b - a;
			//dotRes = calc * calc;
			//XMStoreFloat3(&res, dotRes);
			//ans = res.x + res.y + res.z;

			XMStoreFloat3(&res, XMVector3Dot(calc, calc));

			//	XMVECTOR temp = XMVector3Dot(calc, calc);

			if (res.x <= 1000 && res.x > 0)
			{
				collisionsToResolve.push_back(CollisionToRessolve(res.x, XMVector3Normalize(a - b)));
			}
		}
	}

	for (const auto& param : collidableGenerals)
	{
		if (param)
		{
			b = XMLoadFloat3(&param->getSoldierSprite()->getPosition());
			a = XMLoadFloat3(&m_sprite->getPosition());

			calc = b - a;
			//dotRes = calc * calc;
			//XMStoreFloat3(&res, dotRes);
			//ans = res.x + res.y + res.z;

			XMStoreFloat3(&res, XMVector3Dot(calc, calc));

			//	XMVECTOR temp = XMVector3Dot(calc, calc);

			if (res.x <= 1000 && res.x > 0)
			{
				collisionsToResolve.push_back(CollisionToRessolve(res.x, XMVector3Normalize(a - b)));
			}
		}
	}

	if(collisionsToResolve.size() > 0)
		resolveCollisions(collisionsToResolve);
}

void Soldier::resolveCollisions(std::vector<CollisionToRessolve> c2r)
{
	// sort the collisions based on the closest ones first (lowest res value)

	std::sort(c2r.begin(), c2r.end(),[](CollisionToRessolve a, CollisionToRessolve b)
	{
		return a.dotProductValue < b.dotProductValue;
	});

	XMFLOAT3 floatDir;
	XMFLOAT3 temp;

	for(auto& param : c2r)
	{
		// reverse engineer the direction vector to put the position of the sprite back
		XMStoreFloat3(&floatDir, param.direction * 10);
		temp = m_sprite->getPosition();
		temp.x += floatDir.x;
		temp.y += floatDir.y;
		m_sprite->changePosition(temp);
	}
}

void Soldier::takeDamage(const int& dammageToDeduct)
{
	m_health -= dammageToDeduct;
	if(m_health <= 0)
	{
		m_isAlive = false;
	}

}

void Soldier::die()
{
	SpriteManager::s_spriteManager->getSprite(m_spriteID)->changeVisibility(false);
	m_isAlive = false;
	cleanUp();
}

void Soldier::kill()
{
	die();
}

bool Soldier::getIsAlive() const
{
	return m_isAlive;
}