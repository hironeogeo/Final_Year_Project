#pragma once

#include "Soldier.h"
#include "General.h"

class Sergent : public Soldier
{
private:

public:
	Sergent(const XMFLOAT3& pos, Weapon* weapon, Ammunition* ammo, LPWSTR filePath, Team* team);
	virtual ~Sergent();

	virtual void behave();

	void update();
	void render();
};
