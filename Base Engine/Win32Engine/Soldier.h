#pragma once
#include "Sprite.h"
#include "Weapon.h"
#include <d3d11.h>
#include <xnamath.h>
#include "Behaviour.h"
#include "Team.h"

class Ammunition;

//struct CollisionToRessolve
//{
//	float dotProductValue;
//	XMVECTOR direction;
//	CollisionToRessolve(float dotProductVal, XMVECTOR dir)
//	{
//		dotProductValue = dotProductVal;
//		direction = dir;
//	}
//};

class Soldier
{
protected:
	XMFLOAT3 m_pos;
	float m_movement;
	float m_speed;
	float m_defense;
	float m_firepower;
	int m_leadership;
	Sprite* m_sprite;
	Weapon* m_weapon;
	Behaviour m_currentBehaviour;
	Team* m_team;
	// the type of ammo that this soldier will use
	Ammunition* m_ammo;
	// essentially max ammo the soldier will
	const int m_maxAmmo;
	int m_spriteID;
	bool m_hasAmmo;
	int m_health;
	bool m_isAlive;
	void resolveCollisions(std::vector<CollisionToRessolve> c2r);
	void die();



	// a vector to store the soldiers active bullets
	//std::vector<Ammunition*> m_ammuntionVector;

public:
	//Soldier(LPWSTR spriteName);
	Soldier(LPWSTR spriteBodyName, Weapon* weapon, Ammunition* ammo, const XMFLOAT3& pos, const float& movement, 
			const float& speed, const float& defense, const float firepower, const int& leadership, Behaviour startingBehaviour, 
			Team* team, int maxAmmo, const int& health);
	virtual ~Soldier() = 0;

	virtual void behave() = 0;

	void update();
	void render();

	void cleanUp();
	
	XMFLOAT3 getSoldierPosition();
	Weapon* getSoldierWeapon();
	Behaviour* getSoldierBehaviour();
	Ammunition* getSoldierAmmo();
	Sprite* getSoldierSprite();
	Team* getSoldierTeam();
	int getMaxAmmo() const;
	void processCollisions();
	void takeDamage(const int& dammageToDeduct);
	void kill();
	bool getIsAlive() const;

	
};
