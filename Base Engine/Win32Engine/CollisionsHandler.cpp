#include "CollisionsHandler.h"

void CollisionsHandler::generateBoundingSprites()
{
	for(auto& param : SpriteManager::s_spriteManager->getAllSprites())
	{
		param->getBoundingSprite()->m_position = XMLoadFloat3(&param->getPosition());

		XMFLOAT3 position = param->getPosition();
		XMFLOAT3 bound = position;
		bound.x -= param->getBoundingSprite()->m_spriteWidth;
		
		param->getBoundingSprite()->m_leftEdgeBound = XMLoadFloat3(&bound);

		bound = position;
		bound.x += param->getBoundingSprite()->m_spriteWidth;
		param->getBoundingSprite()->m_rightEdgeBound = XMLoadFloat3(&bound);
	}
}

void CollisionsHandler::proecssSpriteToSprite()
{
	generateBoundingSprites();


}