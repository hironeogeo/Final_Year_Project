#pragma once

#include "Arrow.h"

class StandardArrow : public Arrow
{
private:

public:
	StandardArrow(LPWSTR spriteFilePath , const int& direcion);
	~StandardArrow();

	//virtual void fire();s

	void update();
	void render();

};
