#pragma once

#include "Dx11DemoBase.h"

class TriangleDemo : public Dx11DemoBase
{
private:
	ID3D11VertexShader* solidColorVS_;
	ID3D11PixelShader* solidColorPS_;

	ID3D11InputLayout* inputLayout_;
	ID3D11Buffer* vertexBuffer_;

public:
	TriangleDemo();
	virtual ~TriangleDemo();
	
	virtual bool loadContent();
	virtual void unloadContent();

	void update(float dt);
	void render();

};
