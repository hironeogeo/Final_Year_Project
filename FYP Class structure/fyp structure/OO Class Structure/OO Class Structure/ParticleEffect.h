#pragma once
#include "Sprite.h"
class ParticleEffect
{
private:
	float m_lifeTime;
	Sprite m_sprite;

public:
	ParticleEffect();
	~ParticleEffect();
};
