#pragma once
#include <string>

class Sound
{
private:
	float m_duration;
	int m_id;
	int m_volume;
	std::string m_fileName;

public:
	Sound();
	Sound(float duration, int id, int volume, std::string filename);
	~Sound();

	float getDuration();
	int getID();
	int getVolume();
	std::string getFileName();

	void setDuration(const float& newDuration);
	void setID(const int& newID);
	void setVolume(const int& newVolume);
	void setFileName(std::string newFileName);

};
