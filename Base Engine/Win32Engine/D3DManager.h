#pragma once

#include <d3d11.h>
#include <d3dx11.h>
#include <DxErr.h>
#include <d3dcompiler.h>

class D3DManager
{
private:
	ID3D11Device* m_d3dDevice;
	ID3D11DeviceContext* m_d3dContext;
	IDXGISwapChain* m_swapChain;
	ID3D11RenderTargetView* m_backBufferTarget;
	D3D_DRIVER_TYPE m_driverType;
	D3D_FEATURE_LEVEL m_featureLevel;

	HINSTANCE m_hInstance;
	HWND m_hwnd;

public:

	static D3DManager* s_d3dManager;

	D3DManager();
	~D3DManager();
	// setup d3d for the project
	bool initialise(HINSTANCE hInstance, HWND hwnd);
	bool compiled3dShader(LPCWSTR filePath, char* entry, char* shaderModel, ID3DBlob** buffer);
	void shutdown();

	// getters
	ID3D11Device*			getD3DDevice();
	ID3D11DeviceContext*	getD3DContext();
	IDXGISwapChain*			getSwapChain();
	ID3D11RenderTargetView*	getBackBufferTarget();
	D3D_DRIVER_TYPE			getDriverType();
	D3D_FEATURE_LEVEL		getFeatureLevel();

};
