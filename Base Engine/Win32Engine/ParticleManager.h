#pragma once

#include "Particle.h"
#include <vector>

class ParticleManager
{
private:
	std::vector<Particle*> m_particles;
	
public:
	ParticleManager();
	~ParticleManager();
	static ParticleManager* s_ParticleManager;

	void updateParticles();
	void addParticle(XMFLOAT3 pos);



};
