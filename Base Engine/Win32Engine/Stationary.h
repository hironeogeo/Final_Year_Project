#pragma once

#include "Weapon.h"

// class to be the base for all stationary type weapons
class Stationary : public virtual Weapon
{
protected:
	//we need some sort of mechanism to represent the attack radius of the weapon
	float m_attackRadius;
	int m_attackDuration;
	bool m_isAttackPos;
	XMFLOAT3 m_weaponPosition;


public:
	Stationary(const float& damage, const float& speed, const int& refrershTime, const float& attackRadius);
	~Stationary();

	virtual void attack();

	void update();
	void render();

};
