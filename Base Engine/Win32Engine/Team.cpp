#include "Recruit.h"
#include "Sergent.h"
#include "General.h"

#include "Team.h"

Team::Team(Facing facing, XMFLOAT2 min, XMFLOAT2 max, const int& numStartingSoldiers)
	: m_directionFacing(facing), m_otherTeam(nullptr), m_teamAreaMin(min), m_teamAreaMax(max), m_startingSoldiers(numStartingSoldiers)
{

}

Team::~Team()
{

}

void Team::addRecruit(XMFLOAT3 pos, Weapon* w, Ammunition* ammo)
{
	if(m_directionFacing == Facing::LEFT)
		m_recruits.push_back(new Recruit(pos, w, ammo, L"Assets/Sprites/Basic_Soldier_left_head.png", this));
	else
		m_recruits.push_back(new Recruit(pos, w, ammo, L"Assets/Sprites/Basic_Soldier_right_head.png", this));
}

void Team::addSergent(XMFLOAT3 pos, Weapon* w, Ammunition* ammo)
{
	if(m_directionFacing == Facing::LEFT)
		m_sergents.push_back(new Sergent(pos, w, ammo, L"Assets/Sprites/Medium_Soldier_left_head.png",  this));
	else
		m_sergents.push_back(new Sergent(pos, w, ammo, L"Assets/Sprites/Medium_Soldier_right_head.png", this));
}

void Team::addGeneral(XMFLOAT3 pos, Weapon* w, Ammunition* ammo)
{
	if (m_directionFacing == Facing::LEFT)
		m_generals.push_back(new General(pos, w, ammo, L"Assets/Sprites/Leader_Soldier_left_head.png", this));
	else
		m_generals.push_back(new General(pos, w, ammo, L"Assets/Sprites/Leader_Soldier_right_head.png", this));
}

Sergent* Team::findNearestSergent(XMFLOAT3 base)
{
	XMFLOAT3 closest = XMFLOAT3(999999.0f, 999999.0f, 999999.0f);
	XMFLOAT3 basePos;
	XMFLOAT3 testPos;
	XMFLOAT3 temp;
	Sergent* closestSergent = nullptr;
	XMVECTOR tempVec;

	if (m_sergents.size() > 0)
	{
		for (const auto& param : m_sergents)
		{
			if (param)
			{
				//basePos = base;
				testPos = param->getSoldierPosition();

				tempVec = XMLoadFloat3(&testPos) - XMLoadFloat3(&base);
				XMStoreFloat3(&temp, tempVec);

				if (temp.x < closest.x && temp.y < closest.y)
				{
					closest = temp;
					closestSergent = param;
				}
			}
		}
		return closestSergent;
	}
	else
		return nullptr;
}

General* Team::findNearestGeneral(XMFLOAT3 base)
{
	XMFLOAT3 closest = XMFLOAT3(999999.0f, 999999.0f, 999999.0f);
	XMFLOAT3 basePos;
	XMFLOAT3 testPos;
	XMFLOAT3 temp;
	General* closestGeneral = nullptr;
	XMVECTOR tempVec;

	if (m_generals.size() > 0)
	{
		for (const auto& param : m_generals)
		{
			if (param)
			{
				//basePos = base;
				testPos = param->getSoldierPosition();

				tempVec = XMLoadFloat3(&testPos) - XMLoadFloat3(&base);
				XMStoreFloat3(&temp, tempVec);

				if (temp.x < closest.x && temp.y < closest.y)
				{
					closest = temp;
					closestGeneral = param;
				}
			}
		}
		return closestGeneral;
	}
	else
		return nullptr;
}

size_t Team::getNumRecruits() const
{
	return m_recruits.size();
}

size_t Team::getNumSergents() const
{
	return m_sergents.size();
}

size_t Team::getNumGenerals() const
{
	return m_generals.size();
}

size_t Team::getNumTeamMembers() const 
{
	return getNumRecruits() + getNumSergents() + getNumGenerals();
}

Facing Team::getDirectionFacing() const
{
	return m_directionFacing;
}

void Team::updateSoldiers()
{
	for(auto& param : m_generals)
	{
		if (param)
		{
			if (param->getIsAlive())
			{
				param->getSoldierWeapon()->update();
				param->getSoldierBehaviour()->update();
				param->update();

				if (!param->getIsAlive())
				{
					param->kill();
					delete param;
					param = nullptr;
				}
			}
			else
			{
				param->kill();
				delete param;
				param = nullptr;
			}
		}
	}

	for(auto& param : m_sergents)
	{
		if (param)
		{
			if (param->getIsAlive())
			{
				param->getSoldierWeapon()->update();
				param->getSoldierBehaviour()->update();
				param->update();

				if (!param->getIsAlive())
				{
					param->kill();
					delete param;
					param = nullptr;
				}
			}
			else
			{
				param->kill();
				delete param;
				param = nullptr;
			}
		}
	}

	for(auto& param : m_recruits)
	{
		if (param)
		{
			if (param->getIsAlive())
			{
				param->getSoldierWeapon()->update();
				param->getSoldierBehaviour()->update();
				param->update();

				if (!param->getIsAlive())
				{
					param->kill();
					delete param;
					param = nullptr;
				}
			}
			else
			{
				param->kill();
				delete param;
				param = nullptr;
			}
		}
	}
}

void Team::cleanUpTeam()
{
	for (auto& param : m_generals)
	{
		if (param)
		{
			param->cleanUp();
			delete param;
			param = nullptr;
		}
	}

	for (auto& param : m_sergents)
	{
		if (param)
		{
			param->cleanUp();
			delete param;
			param = nullptr;
		}
	}

	for (auto& param : m_recruits)
	{
		if (param)
		{
			param->cleanUp();
			delete param;
			param = nullptr;
		}
	}
}

void Team::setOtherTeam(Team* otherTeam)
{
	m_otherTeam = otherTeam;
}

Team* Team::getOtherTeam()
{
	return m_otherTeam;
}

std::vector<Recruit*> Team::getRecruits()
{
	return m_recruits;
}

std::vector<Sergent*> Team::getSergents()
{
	return m_sergents;
}

std::vector<General*> Team::getGenerals()
{
	return m_generals;
}

void Team::setupTeam(const int& numSoldiers)
{
	


}

XMFLOAT2 Team::getMaxArea() const
{
	return m_teamAreaMax;
}

XMFLOAT2 Team::getMinArea() const
{
	return m_teamAreaMin;
}

int Team::getNumStartingSoldiers() const
{
	return m_startingSoldiers;
}
