#pragma once

#include "Stationary.h"
#include "Projectile.h"

class Knife : public Stationary, public Projectile
{
private:

public:
	Knife();
	~Knife();
};
