#pragma once

#include "ThrowableWeapons.h"

class Dagger : public ThrowableWeapons
{
public:
	Dagger();
	~Dagger();

	void attack() override;
};
