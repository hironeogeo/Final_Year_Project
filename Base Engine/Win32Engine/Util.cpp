#include "Util.h"

Util::Util()
{

}

Util::~Util()
{

}

// explanation as to how this function works because you have already
// forgotten how it works at least once already!

// the theory behind this function is that if you subtract the numbers then work out the float division (* 0.5 as it's more efficient)
// then subtract the numbers and work out the integer division 
// next subtract the int from the float
// finally * the float answer by 10 to see if the it was a half

bool Util::checkFloat(const int& num1, const int& num2)
{
	float temp1 = num1;
	float temp2 = num2;

	float fAns = 0.0;
	fAns = (temp2 - temp1);
	fAns *= 0.5; // float version of the answer

	int nAns = 0; 
	nAns = temp2 - temp1;
	nAns *= 0.5; // interger version of the answer

	fAns -= nAns;

	if (fAns * -10 == 5)
	{
		return true;
	}
	else
	{
		return false;
	}
}

// this function is used to work out how much time to add to the next time interval.
int Util::calculateTimeToAdd( int totalTime, const int& boundry, int& remainder)
{
	int time2Add = 0;
	if (totalTime < boundry)
	{
		// no time needs adding so return 0 but pass by reference the remainder back to the caller
		remainder += totalTime;
		return 0;
	}
	else
	{
		// totalTime is bigger than boundry
		while (totalTime > boundry)
		{
			time2Add += 1;
			totalTime -= boundry;
		}
	}
	remainder += totalTime;
	return time2Add;
}

// this function is used to work out how much time to subtract and carry over to the next time interval.
int Util::calculateTimeToMinus(int totalTime, const int& boundry, int& remainder)
{
	const int negativeCheck = 0;
	int time2Minus = 0;
	if (totalTime > negativeCheck)
	{
		// no time needs subtracting so return 0 but pass by reference the remainder back to the caller
		remainder += totalTime;
		return 0;
	}
	else
	{
		// while total time is still < 0 
		while (totalTime < negativeCheck)
		{
			// work out how many boundries it takes to bring the time above 0
			time2Minus += 1;
			totalTime -= boundry;
		}
	}
	remainder += totalTime;
	return time2Minus;
}