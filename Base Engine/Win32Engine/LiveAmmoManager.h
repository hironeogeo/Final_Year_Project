#pragma once
#include "Ammunition.h"
#include <vector>

class LiveAmmoManager
{
private: 
	std::vector<Ammunition*> m_liveAmmo;

public:

	static LiveAmmoManager* s_liveAmmoManager;
	LiveAmmoManager();
	~LiveAmmoManager();

	void addLiveAmmo(Ammunition* newLiveAmmo);
	//Ammunition* getLiveAmmo(int id);
	std::vector<Ammunition*> getAllLiveAmmo();
	void removeDeadAmmo();
	
	void updateLiveAmmo();

};