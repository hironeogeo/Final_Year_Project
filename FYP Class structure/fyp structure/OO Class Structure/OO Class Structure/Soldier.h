#pragma once
#include "Mesh.h"
#include <vector>

class Soldier
{
private:
	Mesh m_mesh;
	std::vector<float> m_pos;
	float m_movement;
	float m_speed;
	float m_defense;
	float firepower;

public:
	Soldier();
	~Soldier();

};
