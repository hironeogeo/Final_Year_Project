#include "Ranged.h"
#include "Soldier.h"

Ranged::Ranged(const float& damage, const float& speed, const int& refreshTime)
	:Weapon(damage, refreshTime,nullptr)
{
	m_launchFactor = speed;
	m_shotsFired = 0;
}

Ranged::~Ranged()
{
	
}

void Ranged::attack()
{
	if (m_shotsFired <= m_owner->getMaxAmmo())
	{
		float damage = m_damage * m_owner->getSoldierAmmo()->getDamage();
		float speed = m_launchFactor * m_owner->getSoldierAmmo()->getSpeed();
		int direction = m_owner->getSoldierTeam()->getDirectionFacing();
		XMFLOAT3 pos = m_owner->getSoldierSprite()->getPosition();

		calculateFiringOffset(pos, direction);

		m_lastUsed.setToCurrentTime();

		incrementShotsFired();

		LiveAmmoManager::s_liveAmmoManager->addLiveAmmo(new Ammunition(m_owner->getSoldierAmmo()->getSpriteName(), damage, m_owner->getSoldierAmmo()->getLifeTime(), speed, direction, m_fireOffset, m_owner));
	}
}

void Ranged::incrementShotsFired()
{
	m_shotsFired += 1;
}

int Ranged::getShotsFired() const
{
	return m_shotsFired;
}


