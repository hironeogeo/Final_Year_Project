#pragma once
#include <ctime>
using namespace std;
#include <string>
#include "Util.h"
#include <iomanip>

class Time
{
private:
	int hours_;
	int minutes_;
	int seconds_;
	bool t4hour_;
	bool isPM_;
	Util toolBox;

public:
	// constructors
	Time();
	Time(const int& h, const int& m, const int& s);
	Time(const int& h, const int& m, const int& s, const bool& twenty4Hours);
	Time(const int& h, const int& m, const int& s, const bool& twenty4Hours, const bool& pm);
	Time(const int& timeInSeconds);
	Time(Time &t);
	
	//deconstructor
	~Time();

	const int getHours();
	const int getMinutes();
	const int getSeconds();
	const bool getTimeFormat();
	const bool getPM();

	void setHours(const int& h);
	void setMinutes(const int& m);
	void setSeconds(const int& s);
	void setTimeFormat(const bool& b);
	void setIsPM(const bool& pm);
	void setTime(const Time& rhs);
	void toggleTimeFormat();
	void isValidTime();

	void setToCurrentTime();
	int convertToSeconds();
	string convertToString();
	Time getCurrentTime();
	Time convertFromSeconds(const int& timeInSeconds);
	Time convertTo24Hours();
	Time convertTo12Hour();
	Time findMidPoint(Time& rhs);
	Time findMidPoint(Time& t1, Time& t2);
	int findMidvalue(const int& value1, const int& value2);
	void checkForPM();
	void checkForDayEnd();
	void removeMinus(int& hours);
	bool isEqiliventTime(Time rhs);

	// operator overloads
	Time operator+(const Time& rhs);
	Time operator-(const Time& rhs);
	void operator=(const Time& rhs);
	void operator=(const Time* rhs);
	bool operator==(const Time& rhs);
	bool operator!=(const Time& rhs);
	void operator++();
	void operator--();
};