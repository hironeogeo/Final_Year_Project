#pragma once

//#include "Sprite.h"
#include "ParticleManager.h"
#include "time.h"
class Team;
class Soldier;

struct CollisionToRessolve
{
	float dotProductValue;
	XMVECTOR direction;
	CollisionToRessolve(float dotProductVal, XMVECTOR dir)
	{
		dotProductValue = dotProductVal;
		direction = dir;
	}
};

class Ammunition
{
private:
	float m_damage;
	float m_lifeTime;
	float m_speed;
	Sprite* m_ammunitionSprite;
	int m_directionOfFire;
	LPWSTR m_spriteName;
	// record of when the ammo was created
	Time m_timeAlive;
	// temp time for calculations
	Time m_tempTime;
	bool m_isAlive;
	int m_diff;
	int m_spriteID;
	//Soldier* m_owner;
	Team* m_otherTeam;
	int m_id;
	Time m_timeCreatedTrailPart;
	bool m_hasTrail;
	const int m_trailInterval = 1;

	void die();
	bool processAmmo2Ammo();
	void processAmmo2Soldier();

public:
	Ammunition(LPWSTR spritePath, const float& damage, const float& lifeTime, const float& speed, const int& direction, Soldier* owner);
	Ammunition(LPWSTR spritePath, const float& damage, const float& lifeTime, const float& speed, const int& direction, XMFLOAT3 startPos, Soldier* owner);

	~Ammunition();

	void update();
	void reset(const XMFLOAT3& soldierPos);

	float getDamage() const;
	float getLifeTime() const;
	float getSpeed() const;
	Sprite* getSprite() const;
	int getDirection() const;
	LPWSTR getSpriteName() const;
	int getAmmoID() const;
	bool getIsAlive() const;
	void setParent(Soldier* newParent);
	void setID(const int& ammoID);
	void kill();
	void processCollisions();

};
