#pragma once

class Behaviour
{
private:
	enum behaviours
	{
		ATTACK = 0,
		RETREAT,
		FORMUP
	};

public:
	Behaviour();
	~Behaviour();


	void changeBehaviour(const int& newBehaviour);
	void behave(); // implement what the behaviours actually mean
	
};