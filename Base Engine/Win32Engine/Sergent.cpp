#include "Sergent.h"

Sergent::Sergent(const XMFLOAT3& pos, Weapon* weapon, Ammunition* ammo, LPWSTR filePath, Team* team)
	:Soldier(filePath, weapon, ammo, pos, 1.0f, 1.5f, 3.0f, 4.0f, 2.0f, Behaviour(DISPURSE), team, 20, 7)
{

}

Sergent::~Sergent()
{

}

void Sergent::behave()
{
	switch (m_currentBehaviour.getType())
	{
	case ATTACK:
	{
		if (m_weapon->getHasRefreshed())
		{
			m_weapon->attack();
		}

		if (m_team->getDirectionFacing() == LEFT)
		{
			XMFLOAT3 tmp = m_sprite->getPosition();
			tmp.x -= 0.025 + (m_speed + m_movement);
			m_sprite->changePosition(tmp);
		}
		else
		{
			XMFLOAT3 tmp = m_sprite->getPosition();
			tmp.x += 0.025f + (m_speed + m_movement);
			m_sprite->changePosition(tmp);
		}
	}

	//m_currentBehaviour = FORMUP;
	break;
	case RETREAT:
	{
		// move away from the enemy
		if (m_team->getDirectionFacing() == LEFT)
		{
			XMFLOAT3 tmp = m_sprite->getPosition();
			tmp.x += 0.05f + (m_speed + m_movement);
			m_sprite->changePosition(tmp);
		}
		else
		{
			XMFLOAT3 tmp = m_sprite->getPosition();
			tmp.x -= 0.05f + (m_speed + m_movement);
			m_sprite->changePosition(tmp);
		}
	}
	break;

	case DISPURSE:
	{
		if (m_team->getNumGenerals() > 0)
		{
			General* closest = m_team->findNearestGeneral(m_pos);

			if (closest != nullptr)
			{
				XMVECTOR temp;
				temp = XMLoadFloat3(&closest->getSoldierPosition()) - XMLoadFloat3(&m_pos);
				XMVector3Normalize(temp);

				XMFLOAT3 newPos;
				XMStoreFloat3(&newPos, temp);
				newPos.x *= 0.0005;
				newPos.y *= 0.0005;

				m_pos.x -= newPos.x;
				m_pos.y -= newPos.y;

				m_sprite->changePosition(m_pos);
			}
		}
	}
	break;

	case FORMUP:
	{
		// look for the closest sergent and move towards them

		General* closest = m_team->findNearestGeneral(m_pos);
		if (closest != nullptr)
		{
			XMVECTOR temp;
			temp = XMLoadFloat3(&closest->getSoldierPosition()) - XMLoadFloat3(&m_pos);
			XMVector3Normalize(temp);

			XMFLOAT3 newPos;
			XMStoreFloat3(&newPos, temp);
			newPos.x *= 0.0005;
			newPos.y *= 0.0005;

			m_pos.x += newPos.x;
			m_pos.y += newPos.y;

			m_sprite->changePosition(m_pos);
		}
		else
			m_currentBehaviour.changeBehaviour(ATTACK);
	}

	break;
	case IDLE:// do nothing

		if (m_team->getDirectionFacing() == LEFT)
		{
			XMFLOAT3 tmp = m_sprite->getPosition();
			tmp.y -= 0.05f + (m_speed + m_movement);
			m_sprite->changePosition(tmp);
		}
		else
		{
			XMFLOAT3 tmp = m_sprite->getPosition();
			tmp.y -= 0.05f + (m_speed + m_movement);
			m_sprite->changePosition(tmp);
		}
		break;
	}
}

void Sergent::update()
{
	if (m_isAlive)
	{
		behave();
		processCollisions();

		Soldier::update();
	}
}

void Sergent::render()
{

}