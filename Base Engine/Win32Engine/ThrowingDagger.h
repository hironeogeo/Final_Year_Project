#pragma once

#include "ThrowableAmmo.h"

class ThrowingDagger : public ThrowableAmmo
{
private:

public:
	ThrowingDagger(LPWSTR spriteFilePath, const int& direction);
	~ThrowingDagger();
};
