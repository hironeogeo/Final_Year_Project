#pragma once

#include "Sprite.h"
#include <vector>


class SpriteManager
{
private:
	// vector of in game sprites
	std::vector<Sprite*> m_sprites;

	ID3D11VertexShader* m_spriteVertexShader;
	ID3D11PixelShader* m_spritePixelShader;
	ID3D11InputLayout* m_spriteInputLayout;
	ID3D11SamplerState* m_spriteColourMapSampler;
	ID3D11BlendState* m_spriteAlphaBlendState;
	ID3D11Buffer* m_mvpCB;
	XMMATRIX m_vpMatrix;
	XMMATRIX m_wvpMatrix;
	

public:
	static SpriteManager* s_spriteManager;

	SpriteManager();
	~SpriteManager();
	bool initialise();

	Sprite* addNewSprite(LPWSTR fileName, int& id);
	Sprite* addNewSprite(LPWSTR fileName, int& id, const XMFLOAT3& position);
	Sprite* addNewSprite(LPWSTR fileName, int& id, const XMFLOAT3& position, const XMFLOAT3& scale, const float& rotation);
	Sprite* getSprite(const int& id);
	std::vector<Sprite*> getAllSprites();
	void removeSprite(const int& id);

	void renderSprites();
	void updateSprites();
	
};
