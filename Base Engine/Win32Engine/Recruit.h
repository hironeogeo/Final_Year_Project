#pragma once

#include "Soldier.h"
#include "Sergent.h"
class Recruit : public Soldier
{
private:

public:
	Recruit(const XMFLOAT3& pos, Weapon* weapon, Ammunition* ammo, LPWSTR filePath, Team* team);
	virtual ~Recruit();

	virtual void behave();

	void update();
	void render();
};