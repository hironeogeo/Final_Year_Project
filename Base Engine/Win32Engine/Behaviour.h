#pragma once

#include "Time.h"
#include <map>
#include <algorithm>


enum behaviours
{
	ATTACK = 0, RETREAT, FORMUP, IDLE, DISPURSE
};

struct DurationMinMax
{
	int m_min, m_max;
	DurationMinMax(const int& min, const int& max)
	{
		m_min = min;
		m_max = max;
	}

	DurationMinMax::DurationMinMax()
	{
		m_min = 0;
		m_max = 0;
	}

	~DurationMinMax()
	{
		
	}
};

class Behaviour
{
private:
	behaviours m_type;
	float m_duration;
	void init(behaviours behaviourType);
	Time m_behaviourStarted;
	// time variable for calculations
	Time m_behaviourTmp;
	int m_behaviourDiff;
	std::map<behaviours, DurationMinMax> m_behaviourDurations;


public:
	Behaviour(behaviours behaviourType);
	~Behaviour();

	// allows manual change the behaviour regardless of time
	void changeBehaviour(behaviours newBehaviourType); 

	void update();

	behaviours getType() const;
	float getDuration() const;
};
