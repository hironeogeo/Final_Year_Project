#pragma once

#include "Dx11DemoBase.h"


class TextureDemo : public Dx11DemoBase
{
private:
	ID3D11VertexShader* solidColourVS_;
	ID3D11PixelShader* solidColourPS_;
	ID3D11InputLayout* inputLayout_;
	ID3D11Buffer* vertexBuffer_;

	ID3D11ShaderResourceView* colourMap_;
	ID3D11SamplerState* colourMapSampler_;

public:
	TextureDemo();
	~TextureDemo();

	bool loadContent();
	void unloadContent();

	void update(float dt);
	void render();

};
