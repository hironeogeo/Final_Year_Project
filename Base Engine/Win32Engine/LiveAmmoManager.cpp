#include "LiveAmmoManager.h"

LiveAmmoManager* LiveAmmoManager::s_liveAmmoManager = NULL;

LiveAmmoManager::LiveAmmoManager()
{
	s_liveAmmoManager = this;
}

LiveAmmoManager::~LiveAmmoManager()
{
	for(auto& param : m_liveAmmo)
	{
		delete param;
		param = nullptr;
	}
}

void LiveAmmoManager::addLiveAmmo(Ammunition* newLiveAmmo)
{
	m_liveAmmo.push_back(newLiveAmmo);
	newLiveAmmo->setID(m_liveAmmo.size() - 1);
}

void LiveAmmoManager::updateLiveAmmo()
{
	for (auto& param : m_liveAmmo)
	{
		if (param)
		{
			if (param->getIsAlive())
			{
				param->update();
				if(param->getIsAlive())
					param->processCollisions();
				else
				{
					param->kill(); // really not convinced that this will work!! :(
					delete param;
					param = nullptr;
				}
			}
			else
			{
				param->kill(); // really not convinced that this will work!! :(
				delete param;
				param = nullptr;
			}
		}
	}
}

std::vector<Ammunition*> LiveAmmoManager::getAllLiveAmmo()
{
	return m_liveAmmo;
}

void LiveAmmoManager::removeDeadAmmo()
{
	for (auto& param : m_liveAmmo)
	{
		if (param)
		{
			if (!param->getIsAlive())
			{
				param->kill(); // really not convinced that this will work!! :(
				delete param;
				param = nullptr;
				//	toRemove.push_back(indexToDelete);
					//m_liveAmmo.erase(m_liveAmmo.begin() + indexToDelete);
			}
		}
	}
}

