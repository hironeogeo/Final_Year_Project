#pragma once

#include "Ranged.h"

class FancyBow : public Ranged
{
private:

public:
	FancyBow();
	FancyBow(const float& damage, const float& launchSpeed);
	~FancyBow();

	//virtual void attack();
};

