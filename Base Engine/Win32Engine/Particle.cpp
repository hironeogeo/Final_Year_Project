#include "Particle.h"

Particle::Particle()
	:m_particleSprite(nullptr)
{
	SpriteManager::s_spriteManager->addNewSprite(L"Assets/Sprites/test.png", m_spriteID);
	m_duration = 2;
	m_created.setToCurrentTime();
	m_isAlive = true;
}

Particle::Particle(XMFLOAT3 pos, const int& duration)
	: m_particleSprite(nullptr)
{
	SpriteManager::s_spriteManager->addNewSprite(L"Assets/Sprites/test_new_scale.png", m_spriteID, pos);
	m_duration = duration;
	m_created.setToCurrentTime();
	m_isAlive = true;
}


Particle::Particle(LPWSTR spritePath)
	:m_particleSprite(nullptr)
{
	SpriteManager::s_spriteManager->addNewSprite(spritePath, m_spriteID);
	m_duration = 2;
	m_created.setToCurrentTime();
	m_isAlive = true;
}

Particle::Particle(LPWSTR spritePath, XMFLOAT3 pos)
	: m_particleSprite(nullptr)
{
	SpriteManager::s_spriteManager->addNewSprite(spritePath, m_spriteID, pos);
	m_duration = 2;
	m_created.setToCurrentTime();
	m_isAlive = true;
}

Particle::~Particle()
{
	m_particleSprite = nullptr;
	m_duration = 0;
}

void Particle::update()
{
	if (m_isAlive)
	{
		m_tmp.setToCurrentTime();

		if (Time(m_tmp - m_created).getSeconds() >= m_duration)
		{
			m_isAlive = false;
			SpriteManager::s_spriteManager->getSprite(m_spriteID)->toggleVisibility();
		}
	}
}


Sprite* Particle::getParticleSprite()
{
	return SpriteManager::s_spriteManager->getSprite(m_spriteID);
}

int Particle::getParticleDuration() const
{
	return m_duration;
}

Time* Particle::getTimeCreated()
{
	return &m_created;
}

bool Particle::getIsAlive() const
{
	return m_isAlive;
}


