#include "FancyBow.h"
#include "Soldier.h"

FancyBow::FancyBow()
	:Ranged(2.5f, 2.0f, 3), Weapon(2.5f, 2, nullptr)
{
	
}

FancyBow::FancyBow(const float& damage, const float& launchSpeed)
	:Ranged(damage, launchSpeed, 3), Weapon(damage, 3, nullptr)
{

}

FancyBow::~FancyBow()
{

}

////void FancyBow::attack(const XMFLOAT3& pos, const int& direction, Ammunition ammoToFire)
//void FancyBow::attack()
//{
//	// this is where the "fire()" functionality is going to take place
//
//	//float damage = m_damage * ammoToFire.getDamage();
//	//float speed = m_launchFactor * ammoToFire.getSpeed();
//
//	//float damage = m_owner->getSoldierAmmo()->getDamage();
//	//float speed = m_owner->getSoldierAmmo()->getSpeed();
//
//	float damage = m_damage * m_owner->getSoldierAmmo()->getDamage();
//	float speed = m_launchFactor * m_owner->getSoldierAmmo()->getSpeed();
//	int direction = m_owner->getSoldierTeam()->getDirectionFacing();
//	XMFLOAT3 pos = m_owner->getSoldierSprite()->getPosition();
//
//	calculateFiringOffset(pos, direction);
//
//	m_lastUsed.setToCurrentTime();
//
//	//LiveAmmoManager::s_liveAmmoManager->addLiveAmmo(new Ammunition(ammoToFire.getSpriteName(), damage, ammoToFire.getLifeTime(), speed, direction, m_fireOffset, m_owner));
//
//	LiveAmmoManager::s_liveAmmoManager->addLiveAmmo(new Ammunition(m_owner->getSoldierAmmo()->getSpriteName(), damage, m_owner->getSoldierAmmo()->getLifeTime(), speed, direction, m_fireOffset, m_owner));
//
//	//return new Ammunition(ammoToFire.getSpriteName(), damage, ammoToFire.getLifeTime(), speed, direction, m_fireOffset, m_owner);
//}
