#pragma once

class Collision
{
private:

public:
	Collision();
	~Collision();
	void processCollision();
	void checkCollisions();
	void resolveCollsions();
};