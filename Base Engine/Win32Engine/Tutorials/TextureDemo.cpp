#include "TextureDemo.h"
#include <minwinbase.h>
#include <xnamath.h>


struct VertexPos
{
	XMFLOAT3 pos;
	XMFLOAT2 tex0;
};

TextureDemo::TextureDemo()
	:solidColourVS_(0), solidColourPS_(0), inputLayout_(0), vertexBuffer_(0), colourMap_(0), colourMapSampler_(0)
{
	
}

TextureDemo::~TextureDemo()
{
	
}

bool TextureDemo::loadContent()
{
	ID3DBlob* vsBuffer = 0;
	bool compileResult = compiled3dShader(L"triangleTexture.fx", "VS_Main", "vs_4_0", &vsBuffer);

	if (compileResult == false)
	{
		MessageBox(0, L"error loading vertex shader", L"compilerError", MB_OK);
	}

	HRESULT d3dResult;
	d3dResult = d3dDevice_->CreateVertexShader(vsBuffer->GetBufferPointer(), vsBuffer->GetBufferSize(), 0, &solidColourVS_);

	if (FAILED(d3dResult))
	{
		DXTRACE_MSG(L"Error creating the vertex shader!");

		if (vsBuffer)
		{
			vsBuffer->Release();
		}
		return false;
	}

	D3D11_INPUT_ELEMENT_DESC solidColorLayout[] = 
	{
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0}
	};

	unsigned int totalLayoutElements = ARRAYSIZE(solidColorLayout);

	d3dResult = d3dDevice_->CreateInputLayout(solidColorLayout, totalLayoutElements, vsBuffer->GetBufferPointer(), vsBuffer->GetBufferSize(), &inputLayout_);

	vsBuffer->Release();

	if (FAILED(d3dResult))
	{
		DXTRACE_MSG(L"Error creating the input layout!");
		return false;
	}
	
	// setup the pixel shader

	ID3DBlob* psBuffer = 0;
	compileResult = compiled3dShader(L"triangleTexture.fx", "PS_Main", "ps_4_0", &psBuffer);

	if (compileResult == false)
	{
		MessageBox(0, L"error loading pixel shader", L"compilerError", MB_OK);
	}

	d3dResult = d3dDevice_->CreatePixelShader(psBuffer->GetBufferPointer(), psBuffer->GetBufferSize(), 0, &solidColourPS_);

	psBuffer->Release();

	if (FAILED(d3dResult))
	{
		DXTRACE_MSG(L"Error creating pixel shader!");
		return false;
	}

	VertexPos vertices[] =
	{
		{XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT2(1.0f,1.0f)},
		{XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT2(1.0f,0.0f) },
		{XMFLOAT3(-1.0f, -1.0f,1.0f), XMFLOAT2(0.0f,0.0f) },

		{XMFLOAT3(-1.0f, -1.0f,1.0f), XMFLOAT2(0.0f,0.0f) },
		{XMFLOAT3(-1.0f, 1.0f,1.0f), XMFLOAT2(0.0f,1.0f) },
		{XMFLOAT3(1.0f, 1.0f,1.0f), XMFLOAT2(1.0f,1.0f) },
	};

	D3D11_BUFFER_DESC vertexDesc;
	ZeroMemory(&vertexDesc, sizeof(vertexDesc));
	vertexDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	int numVerts = ARRAYSIZE(vertices);
	vertexDesc.ByteWidth = sizeof(VertexPos) * numVerts;

	D3D11_SUBRESOURCE_DATA resourceData;
	ZeroMemory(&resourceData, sizeof(resourceData));
	resourceData.pSysMem = vertices;

	d3dResult = d3dDevice_->CreateBuffer(&vertexDesc, &resourceData, &vertexBuffer_);

	if (FAILED(d3dResult))
	{
		return false;
	}

	d3dResult = D3DX11CreateShaderResourceViewFromFile(d3dDevice_, L"decal.dds", 0, 0, &colourMap_, 0);

	if(FAILED(d3dResult))
	{
		DXTRACE_MSG(L"Failed to load the texture image!");
		return false;
	}

	D3D11_SAMPLER_DESC colourMapDesc;
	ZeroMemory(&colourMapDesc, sizeof(colourMapDesc));
	colourMapDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	colourMapDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	colourMapDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	colourMapDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	colourMapDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	colourMapDesc.MaxLOD = D3D11_FLOAT32_MAX;

	d3dResult = d3dDevice_->CreateSamplerState(&colourMapDesc, &colourMapSampler_);

	if (FAILED(d3dResult))
	{
		DXTRACE_MSG(L"Failed to create colour map sampler state!");
		return false;
	}

	return true;
}

void TextureDemo::unloadContent()
{
	if (colourMapSampler_)
		colourMapSampler_->Release();

	if (colourMap_)
		colourMap_->Release();

	if (solidColourVS_)
		solidColourVS_->Release();

	if (solidColourPS_)
		solidColourPS_->Release();

	if (inputLayout_)
		inputLayout_->Release();

	if (vertexBuffer_)
		vertexBuffer_->Release();

	colourMapSampler_ = 0;
	colourMap_ = 0;
	solidColourVS_ = 0;
	solidColourPS_ = 0;
	inputLayout_ = 0;
	vertexBuffer_ = 0;
}

void TextureDemo::render()
{
	if (d3dContext_ == 0)
		return;

	float clearColour[4] = { 0.0f, 0.0f, 0.25f, 1.0f };
	d3dContext_->ClearRenderTargetView(backBufferTarget_, clearColour);

	unsigned int stride = sizeof(VertexPos);
	unsigned int offset = 0;

	d3dContext_->IASetInputLayout(inputLayout_);
	d3dContext_->IASetVertexBuffers(0, 1, &vertexBuffer_, &stride, &offset);
	d3dContext_->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	d3dContext_->VSSetShader(solidColourVS_, 0, 0);
	d3dContext_->PSSetShader(solidColourPS_, 0, 0);
	d3dContext_->PSSetShaderResources(0, 0, &colourMap_);
	d3dContext_->PSSetSamplers(0, 1, &colourMapSampler_);
	d3dContext_->Draw(6, 0);

	swapChain_->Present(0, 0);
}

void TextureDemo::update(float dt)
{
	
}