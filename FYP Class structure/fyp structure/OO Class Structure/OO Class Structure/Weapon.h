#pragma once
#include <vector>
#include "Mesh.h"
#include "Sound.h"
#include "ParticleEffect.h"
class Player;
class Weapon
{
private:
	std::vector<float> m_pos;
	float m_lifetime;
	float m_damage;
	Mesh* m_mesh;
	Sound m_firingSound;
	Sound m_collisionSound;
	ParticleEffect m_firing;
	ParticleEffect m_impact;
	Player* m_owner;

public:
	Weapon();
	virtual ~Weapon();
	virtual void attack();
};
