#pragma once
#include "Dx11DemoBase.h"

class BlankDemo : public Dx11DemoBase
{
public:
	BlankDemo();
	virtual ~BlankDemo();

	bool loadContent();
	void unloadContent();

	void update(float dt);
	void render();
};
