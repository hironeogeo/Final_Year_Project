#pragma once

#include "Arrow.h"

class FancyArrow : public Arrow
{
private:

public:
	FancyArrow(LPWSTR spriteFilePath, const int& direcion);
	~FancyArrow();

	//virtual void fire();s

	void update();
	void render();

};
