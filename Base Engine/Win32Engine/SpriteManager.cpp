#include "SpriteManager.h"
#include "D3DManager.h"

SpriteManager* SpriteManager::s_spriteManager = NULL;

SpriteManager::SpriteManager()
	:m_spriteVertexShader(nullptr), m_spritePixelShader(nullptr), m_spriteInputLayout(nullptr), 
	 m_spriteColourMapSampler(nullptr), m_spriteAlphaBlendState(nullptr), m_mvpCB(nullptr)
{
	s_spriteManager = this;
}

SpriteManager::~SpriteManager()
{
	if (m_spriteColourMapSampler)
		m_spriteColourMapSampler->Release();
	
	if (m_spriteVertexShader)
		m_spriteVertexShader->Release();

	if (m_spritePixelShader)
		m_spritePixelShader->Release();

	if (m_spriteInputLayout)
		m_spriteInputLayout->Release();

	if (m_mvpCB)
		m_mvpCB->Release();

	if (m_spriteAlphaBlendState)
		m_spriteAlphaBlendState->Release();

	m_spriteColourMapSampler = nullptr;
	m_spriteVertexShader = nullptr;
	m_spritePixelShader = nullptr;
	m_spriteInputLayout = nullptr;
	m_mvpCB = nullptr;
	m_spriteAlphaBlendState = nullptr;

	for(auto& param : m_sprites)
	{
		if (param)
		{
			delete param;
			param = nullptr;
		}
	}

}

bool SpriteManager::initialise()
{
	//setup the sprite vertex buffer
#pragma region vertex buffer

	ID3DBlob* vsBuffer = 0;
	bool compileResult = D3DManager::s_d3dManager->compiled3dShader(L"Assets/Shaders/SpriteShader.fx", "VS_Main", "vs_4_0", &vsBuffer);

	if (compileResult == false)
	{
		DXTRACE_MSG(L"Error compiling the vertex shader!");
		return false;
	}

	HRESULT d3dResult;

	d3dResult = D3DManager::s_d3dManager->getD3DDevice()->CreateVertexShader(vsBuffer->GetBufferPointer(), vsBuffer->GetBufferSize(),
		0, &m_spriteVertexShader);

	if (FAILED(d3dResult))
	{
		DXTRACE_MSG(L"Error creating the vertex shader!");

		if (vsBuffer)
			vsBuffer->Release();

		return false;
	}

#pragma endregion 

	// setup the input layout for the vertex shader
#pragma region input layout
	D3D11_INPUT_ELEMENT_DESC spriteShaderLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	unsigned int totalLayoutElements = ARRAYSIZE(spriteShaderLayout);

	d3dResult = D3DManager::s_d3dManager->getD3DDevice()->CreateInputLayout(spriteShaderLayout, totalLayoutElements, vsBuffer->GetBufferPointer(), vsBuffer->GetBufferSize(), &m_spriteInputLayout);

	vsBuffer->Release();

	if (FAILED(d3dResult))
	{
		DXTRACE_MSG(L"Error creating the input layout!");
		return false;
	}

#pragma endregion 

	// setup the pixel shader
#pragma region pixel shader
	ID3DBlob* psBuffer = 0;

	compileResult = D3DManager::s_d3dManager->compiled3dShader(L"Assets/Shaders/SpriteShader.fx", "PS_Main", "ps_4_0", &psBuffer);

	if (compileResult == false)
	{
		DXTRACE_MSG(L"Error compiling pixel shader!");
		return false;
	}

	d3dResult = D3DManager::s_d3dManager->getD3DDevice()->CreatePixelShader(psBuffer->GetBufferPointer(),
		psBuffer->GetBufferSize(), 0, &m_spritePixelShader);

	psBuffer->Release();

	if (FAILED(d3dResult))
	{
		DXTRACE_MSG(L"Error creating pixel shader!");
		return false;
	}

#pragma endregion
	
	// setup the colour map sampler
#pragma region colour map sampler

	D3D11_SAMPLER_DESC colourMapDesc;
	ZeroMemory(&colourMapDesc, sizeof(colourMapDesc));
	colourMapDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	colourMapDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	colourMapDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	colourMapDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	colourMapDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	colourMapDesc.MaxLOD = D3D11_FLOAT32_MAX;

	d3dResult = D3DManager::s_d3dManager->getD3DDevice()->CreateSamplerState(&colourMapDesc, &m_spriteColourMapSampler);
	//d3dResult = d3dDevice_->CreateSamplerState(&colorMapDesc, &colorMapSampler_);

	if (FAILED(d3dResult))
	{
		DXTRACE_MSG(L"Failed to create color map sampler state!");
		return false;
	}

#pragma endregion 

	// setup the constant buffer
#pragma region constant buffer
	
	D3D11_BUFFER_DESC constDesc;
	ZeroMemory(&constDesc, sizeof(constDesc));
	constDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constDesc.ByteWidth = sizeof(XMMATRIX);
	constDesc.Usage = D3D11_USAGE_DEFAULT;

	// d3dResult = d3dDevice_->CreateBuffer(&constDesc, 0, &mvpCB_);

	d3dResult = D3DManager::s_d3dManager->getD3DDevice()->CreateBuffer(&constDesc, 0, &m_mvpCB);

	if (FAILED(d3dResult))
	{
		return false;
	}

#pragma endregion 

	// setup the blend state
#pragma region blend State
	
	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(blendDesc));
	blendDesc.RenderTarget[0].BlendEnable = TRUE;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = 0x0F;

	float blendFactor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

	D3DManager::s_d3dManager->getD3DDevice()->CreateBlendState(&blendDesc, &m_spriteAlphaBlendState);
	D3DManager::s_d3dManager->getD3DContext()->OMSetBlendState(m_spriteAlphaBlendState, blendFactor, 0xFFFFFFFF);

#pragma endregion
	
	XMMATRIX view = XMMatrixIdentity();
	XMMATRIX projection = XMMatrixOrthographicOffCenterLH(0.0f, 1280.0f, 0.0f, 960.0f, 0.1f, 100.0f);
	m_vpMatrix = XMMatrixMultiply(view, projection);

	return true;
}

Sprite* SpriteManager::addNewSprite(LPWSTR fileName, int& id)
{
	m_sprites.push_back(new Sprite(fileName));
	id = m_sprites.size() - 1;
	getSprite(m_sprites.size() - 1)->toggleVisibility();
	return getSprite(id);
}

Sprite* SpriteManager::addNewSprite(LPWSTR fileName, int& id, const XMFLOAT3& position)
{
	m_sprites.push_back(new Sprite(fileName, position));
	id = m_sprites.size() - 1;
	return getSprite(id);
}

Sprite* SpriteManager::addNewSprite(LPWSTR fileName, int& id, const XMFLOAT3& position, const XMFLOAT3& scale, const float& rotation)
{
	m_sprites.push_back(new Sprite(fileName, position, scale, rotation));
	id = m_sprites.size() - 1;
	return getSprite(id);
}

Sprite* SpriteManager::getSprite(const int& id)
{
	return m_sprites[id];
}

std::vector<Sprite*> SpriteManager::getAllSprites()
{
	return m_sprites;
}

void SpriteManager::removeSprite(const int& id)
{
	delete m_sprites[id];
	m_sprites[id] = nullptr;
	m_sprites.erase(m_sprites.begin() + id);
}

void SpriteManager::updateSprites()
{
	for(const auto& param : m_sprites)
	{
		param->update();
	}
}

void SpriteManager::renderSprites()
{
	if (D3DManager::s_d3dManager->getD3DContext() == 0)
		return;

	float clearColor[4] = { 0.0f, 0.0f, 0.25f, 1.0f };
	D3DManager::s_d3dManager->getD3DContext()->ClearRenderTargetView(D3DManager::s_d3dManager->getBackBufferTarget(), clearColor);

	unsigned int stride = sizeof(Sprite::VertexPos);
	unsigned int offset = 0;

	D3DManager::s_d3dManager->getD3DContext()->IASetInputLayout(m_spriteInputLayout);
	D3DManager::s_d3dManager->getD3DContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	D3DManager::s_d3dManager->getD3DContext()->VSSetShader(m_spriteVertexShader, 0, 0);
	D3DManager::s_d3dManager->getD3DContext()->PSSetShader(m_spritePixelShader, 0, 0);
	D3DManager::s_d3dManager->getD3DContext()->PSSetSamplers(0, 1, &m_spriteColourMapSampler);
	D3DManager::s_d3dManager->getD3DContext()->VSSetConstantBuffers(0, 1, &m_mvpCB);
	
	for (const auto& param : m_sprites)
	{
		if (param->getVisibility())
		{
			ID3D11Buffer* vb = param->getVertexBuffer();
			ID3D11ShaderResourceView* shaderResource = param->getShaderResource();

			D3DManager::s_d3dManager->getD3DContext()->IASetVertexBuffers(0, 1, &vb, &stride, &offset);
			D3DManager::s_d3dManager->getD3DContext()->PSSetShaderResources(0, 1, &shaderResource);

			XMMATRIX world = param->getWorldMatrix();
			m_wvpMatrix = XMMatrixMultiply(world, m_vpMatrix);
			m_wvpMatrix = XMMatrixTranspose(m_wvpMatrix);

			D3DManager::s_d3dManager->getD3DContext()->UpdateSubresource(m_mvpCB, 0, 0, &m_wvpMatrix, 0, 0);

			param->render();
		}
	}
	//D3DManager::s_d3dManager->getSwapChain()->Present(0, 0);
}



