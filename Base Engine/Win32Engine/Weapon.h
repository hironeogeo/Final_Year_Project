#pragma once
//#include "Sprite.h"
#include "SpriteManager.h"
#include "Ammunition.h"
#include "Time.h"
#include "LiveAmmoManager.h"

class Soldier;


class Weapon
{
protected:
	//Sprite* m_weaponSprite;
	//float m_lifetime;
	float m_damage; // power - to act as a multiplyer for the ammunition
	
	// a time variable to make the time the weapon was last used
	Time m_lastUsed;
	// time variable for calculations
	Time m_tmp;
	// time takes for a weapon to reload;
	int m_refreshTime;
	bool m_hasRefereshed;
	int m_diff;
	// offset to make firing look more realistic
	XMFLOAT3 m_fireOffset;
	Soldier* m_owner;


public:
	

	Weapon(const float& damage, const int& refrershTime, Soldier* owner);
	virtual ~Weapon();
	// this needs to have it's parameters overwritten somehow because stationary weapons don't have ammo
	//virtual void attack(const XMFLOAT3& pos, const int& direction, Ammunition ammoToFire) = 0; 
	virtual void attack() = 0;
	bool getHasRefreshed() const;
	void update();
	XMFLOAT3 getFiringOffset() const;
	void calculateFiringOffset(const XMFLOAT3& pos, const int& direction);
	void updateLastUse();
	void setParent(Soldier* newParent);

};
