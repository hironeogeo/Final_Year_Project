#include "RNG.h"

RNG* RNG::s_RNG = NULL;

RNG::RNG()
	:m_random_number_engine(time(0))
{
	//m_random_number_engine = time(0);
	s_RNG = this;
}

RNG::~RNG()
{
	
}

int RNG::generateRandomNumber(const int& min, const int& max)
{
	//std::mt19937 random_number_engine(time(0));
	std::uniform_int_distribution<int> distribution(min, max);
	return distribution(m_random_number_engine);
}