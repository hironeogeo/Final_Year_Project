#pragma once

#include <vector>
#include "Ammunition.h"
#include "RNG.h"

class Recruit;
class Sergent;
class General;

enum Facing
{ LEFT = 0, RIGHT };

class Team
{
private:
	std::vector<Recruit*> m_recruits;
	std::vector<Sergent*> m_sergents;
	std::vector<General*> m_generals;

	Facing m_directionFacing;
	Team* m_otherTeam;
	XMFLOAT2 m_teamAreaMin;
	XMFLOAT2 m_teamAreaMax;
	const int m_startingSoldiers;
	
public:
	Team(Facing facing, XMFLOAT2 min, XMFLOAT2 max, const int& numStartingSoldiers);
	~Team();

	void addRecruit(XMFLOAT3 pos, Weapon* w, Ammunition* ammo);
	void addSergent(XMFLOAT3 pos, Weapon* w, Ammunition* ammo);
	void addGeneral(XMFLOAT3 pos, Weapon* w, Ammunition* ammo);

	void setupTeam(const int& numSoldiers);

	Sergent* findNearestSergent(XMFLOAT3 base);
	General* findNearestGeneral(XMFLOAT3 base);

	size_t getNumTeamMembers() const;
	size_t getNumRecruits() const;
	size_t getNumSergents() const;
	size_t getNumGenerals() const;

	std::vector<Recruit*> getRecruits();
	std::vector<Sergent*> getSergents();
	std::vector<General*> getGenerals();

	Facing getDirectionFacing() const;

	void updateSoldiers();

	void cleanUpTeam();

	void setOtherTeam(Team* otherTeam);
	Team* getOtherTeam();

	XMFLOAT2 getMinArea() const;
	XMFLOAT2 getMaxArea() const;
	int getNumStartingSoldiers() const;
};
