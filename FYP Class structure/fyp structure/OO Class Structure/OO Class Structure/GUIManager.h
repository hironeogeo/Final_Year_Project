#pragma once
#include "Text.h"
#include <vector>

class GUIManager
{
private:
	std::vector<Text*> textContainer;
public:
	GUIManager();
	~GUIManager();
	void update();
	void changText(const int& id, std::string newText);
	void moveText(const int& id, std::vector<float> newPos);
	void render();




};