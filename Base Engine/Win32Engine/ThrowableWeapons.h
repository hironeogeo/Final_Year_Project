#pragma once

#include "Ranged.h"
#include "Stationary.h"
#include "RNG.h"
class Soldier;

class ThrowableWeapons : public Ranged, public Stationary
{
public:
	ThrowableWeapons();
	~ThrowableWeapons();

	void attack() override;

};