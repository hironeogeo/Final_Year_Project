#pragma once

#include "Sound.h"
#include <vector>

class SoundManager
{
private:
	std::vector<Sound*> m_sounds;
public:
	SoundManager();
	~SoundManager();
	void addSound();
	Sound* getSound();
	std::vector<Sound*> getAllSounds();
};
