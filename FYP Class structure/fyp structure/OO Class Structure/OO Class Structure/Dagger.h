#pragma once

#include "Stationary.h"
#include "Projectile.h"

class Dagger : public Stationary, public Projectile
{
private:

public:
	Dagger();
	~Dagger();

};
