#pragma once
#include <vector>

#include "Weapon.h"
using namespace std;

class Projectile : public Weapon
{
private:
	std::vector<float> m_directon;
	std::vector<float> m_acceleration;
	ParticleEffect m_inFlightParticle;
	Sound m_inFlightSound;

	int m_damageRepeatsCount; // how many time the projectile inflicts damage
	bool m_repeatableDamage; // does the project cause repeate damage

public:
	Projectile();
	virtual ~Projectile();

	void update();
	void render();
};