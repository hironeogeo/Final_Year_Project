#pragma once

#include <string>
#include <d3d11.h>
#include <D3DX11.h>
#include <DxErr.h>
#include <xnamath.h>

class Sprite;
struct BoundingSprite
{
	XMVECTOR m_position;
	XMVECTOR m_leftEdgeBound;
	XMVECTOR m_rightEdgeBound;

	Sprite* m_parent;
	int m_spriteWidth;
};

class Sprite
{
private:
	BoundingSprite m_bs;
	XMFLOAT3 m_position;
	XMFLOAT3 m_scale;
	float m_rotation;
	XMMATRIX m_worldMatrix;
	bool m_isVisible;

	ID3D11Buffer* m_spriteVertexBuffer;
	ID3D11ShaderResourceView* m_spriteColourMap;
	
	bool loadContent(LPWSTR spritePath);
	
public:

	struct VertexPos
	{
		XMFLOAT3 pos;
		XMFLOAT2 tex0;
	};

	Sprite(LPWSTR spritePath);
	Sprite(LPWSTR spritePath, const XMFLOAT3& position);
	Sprite(LPWSTR spritePath, const XMFLOAT3& position, const XMFLOAT3& scale, const float& rotation);
	~Sprite();

	void render();
	void update();

	void changePosition(const XMFLOAT3& newPos);
	void changeScale(const XMFLOAT3& newScale);
	void changeRotation(const float& newRot);
	void changeVisibility(bool newVisibility);
	void toggleVisibility();

	XMFLOAT3 getPosition();
	XMFLOAT3 getScale();
	float getRotation();
	bool getVisibility();
	XMMATRIX getWorldMatrix() const;
	ID3D11Buffer* getVertexBuffer() const;
	ID3D11ShaderResourceView* getShaderResource() const;
	BoundingSprite* getBoundingSprite();
	void reset(XMFLOAT3 pos);
	
};
