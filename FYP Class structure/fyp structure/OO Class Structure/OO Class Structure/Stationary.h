#pragma once

#include "Weapon.h"
// base class of all none projectile weapons

class Stationary : public Weapon
{
private:

public:
	Stationary();
	~Stationary();
};