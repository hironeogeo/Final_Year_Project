#include "Soldier.h"
#include "Ammunition.h"
#include "SpriteManager.h"
#include "RNG.h"
#include "Recruit.h"
#include "Sergent.h"
#include "General.h"
#include "Team.h"


Ammunition::Ammunition(LPWSTR spritePath, const float& damage, const float& lifeTime, const float& speed, const int& direction, Soldier* owner = nullptr)
	:m_ammunitionSprite(nullptr)
{
	//m_ammunitionSprite = SpriteManager::s_spriteManager->addNewSprite(spritePath);

	m_spriteName = spritePath;
	m_damage = damage;
	m_lifeTime = lifeTime;
	m_speed = speed;
	m_directionOfFire = direction;
	m_timeAlive = Time();
	m_isAlive = true;
	m_spriteID = NULL;
	m_diff = 0;

	if (owner)
		m_otherTeam = owner->getSoldierTeam()->getOtherTeam();
	else
		m_otherTeam = nullptr;

	m_hasTrail = false;
}

Ammunition::Ammunition(LPWSTR spritePath, const float& damage, const float& lifeTime, const float& speed, const int& direction, XMFLOAT3 startPos, Soldier* owner = nullptr)
	:m_ammunitionSprite(nullptr)
{
	m_ammunitionSprite = SpriteManager::s_spriteManager->addNewSprite(spritePath, m_spriteID, startPos);
	m_spriteName = spritePath;
	m_damage = damage;
	m_lifeTime = lifeTime;
	m_speed = speed;
	m_directionOfFire = direction;
	m_timeAlive = Time();
	m_isAlive = true;
	m_diff = 0;
	
	if (owner)
		m_otherTeam = owner->getSoldierTeam()->getOtherTeam();
	else
		m_otherTeam = nullptr;
	m_hasTrail = false;
}


Ammunition::~Ammunition()
{

}

void Ammunition::update()
{	
	if (m_isAlive) // only perform the update on live ammo
	{
		m_tempTime.setToCurrentTime();
		m_diff = Time(m_tempTime - m_timeAlive).getSeconds();

		if(m_diff >= m_lifeTime)
			m_isAlive = false;

		if(m_hasTrail)
		{
			m_tempTime.setToCurrentTime();
			m_diff = Time(m_tempTime - m_timeCreatedTrailPart).getSeconds();
			if (m_diff >= m_trailInterval)
			{
				ParticleManager::s_ParticleManager->addParticle(m_ammunitionSprite->getPosition());
				m_timeCreatedTrailPart.setToCurrentTime();
			}
			//else
			//{
			//	int result = RNG::s_RNG->generateRandomNumber(0, 1000);
			//	if (result <= 25)
			//	{
			//		ParticleManager::s_ParticleManager->addParticle(m_ammunitionSprite->getPosition());
			//		m_timeCreatedTrailPart.setToCurrentTime();
			//	}
			//}
		}
		else
		{
			m_hasTrail = true;
			ParticleManager::s_ParticleManager->addParticle(m_ammunitionSprite->getPosition());
			m_timeCreatedTrailPart.setToCurrentTime();
		}

		XMFLOAT3 temp;
		if (m_directionOfFire == 0) // left
		{
			temp = m_ammunitionSprite->getPosition();
			temp.x -= m_speed;
		}
		else
		{
			temp = m_ammunitionSprite->getPosition();
			temp.x += m_speed;
		}

		m_ammunitionSprite->changePosition(temp);
	}
}

void Ammunition::die()
{
	SpriteManager::s_spriteManager->getSprite(m_spriteID)->toggleVisibility();
	m_spriteName = L"";
	m_damage = 0.0f;
	m_lifeTime = 0.0f;
	m_speed = 0.0f;
	m_directionOfFire = 0;
	m_timeAlive = Time(0);
	m_isAlive = false;
	m_spriteID = 0;
	m_diff = 0;
	m_otherTeam = nullptr;
}

void Ammunition::reset(const XMFLOAT3& soldierPos)
{
	m_ammunitionSprite->reset(soldierPos);
}

float Ammunition::getDamage() const
{
	return m_damage;
}

int Ammunition::getDirection() const
{
	return m_directionOfFire;
}

float Ammunition::getLifeTime() const
{
	return m_lifeTime;
}

Sprite* Ammunition::getSprite() const
{
	return m_ammunitionSprite; 
}

void Ammunition::kill()
{
	die();
}

float Ammunition::getSpeed() const
{
	return m_speed;
}

LPWSTR Ammunition::getSpriteName() const
{
	return m_spriteName;
}

int Ammunition::getAmmoID() const
{
	return m_id;
}

bool Ammunition::getIsAlive() const
{
	return m_isAlive;
}

void Ammunition::setParent(Soldier* parent)
{
	//m_owner = parent;
	m_otherTeam = parent->getSoldierTeam()->getOtherTeam();
}

void Ammunition::setID(const int& ammoID)
{
	m_id = ammoID;
}

void Ammunition::processCollisions()
{
	if(!processAmmo2Ammo())
	{
		processAmmo2Soldier();
	}
}

bool Ammunition::processAmmo2Ammo()
{
	std::vector<CollisionToRessolve> collisionsToResolve;

	XMVECTOR a, b, calc;
	XMFLOAT3 res;
	bool hasCollided = false;

	std::vector<Ammunition*> collidableAmmo = LiveAmmoManager::s_liveAmmoManager->getAllLiveAmmo();

	for(auto& param : collidableAmmo)
	{
		if (param)
		{
			b = XMLoadFloat3(&param->getSprite()->getPosition());
			a = XMLoadFloat3(&m_ammunitionSprite->getPosition());

			calc = b - a;
			XMStoreFloat3(&res, XMVector3Dot(calc, calc));
			if (res.x <= 1000 && res.x > 0)
			{
				// kill them and make them invisible
				param->kill();
				kill();
				hasCollided = true;
				break;

				// they are both now invisible and not alive
				// the liveAmmoUpdate will delete them on the next update
			}
		}
	}
	return hasCollided;
}

void Ammunition::processAmmo2Soldier()
{
	XMVECTOR a, b, calc;
	XMFLOAT3 res;
	
	std::vector<Recruit*> collidableRecruits;
	std::vector<Sergent*> collidableSergents;
	std::vector<General*> collidableGenerals;

	//collidableRecruits = m_owner->getSoldierTeam()->getOtherTeam()->getRecruits();
	//collidableSergents = m_owner->getSoldierTeam()->getOtherTeam()->getSergents();
	//collidableGenerals = m_owner->getSoldierTeam()->getOtherTeam()->getGenerals();

	collidableRecruits = m_otherTeam->getRecruits();
	collidableSergents = m_otherTeam->getSergents();
	collidableGenerals = m_otherTeam->getGenerals();

	for (const auto& param : collidableRecruits)
	{
		if (param)
		{
			if (m_isAlive && param->getIsAlive())
			{
				b = XMLoadFloat3(&param->getSoldierSprite()->getPosition());
				a = XMLoadFloat3(&m_ammunitionSprite->getPosition());

				calc = b - a;
				XMStoreFloat3(&res, XMVector3Dot(calc, calc));
				if (res.x <= 1000 && res.x > 0)
				{
					param->takeDamage(m_damage);
					kill();
					break;
				}
			}
		}
	}

	for (const auto& param : collidableSergents)
	{
		if (param)
		{
			if (m_isAlive && param->getIsAlive())
			{
				b = XMLoadFloat3(&param->getSoldierSprite()->getPosition());
				a = XMLoadFloat3(&m_ammunitionSprite->getPosition());

				calc = b - a;
				XMStoreFloat3(&res, XMVector3Dot(calc, calc));
				if (res.x <= 1000 && res.x > 0)
				{
					param->takeDamage(m_damage);
					kill();
					break;
				}
			}
		}
	}

	for (const auto& param : collidableGenerals)
	{
		if (param)
		{
			if (m_isAlive && param->getIsAlive())
			{
				b = XMLoadFloat3(&param->getSoldierSprite()->getPosition());
				a = XMLoadFloat3(&m_ammunitionSprite->getPosition());
				
				calc = b - a;
				XMStoreFloat3(&res, XMVector3Dot(calc, calc));
				if (res.x <= 1000 && res.x > 0)
				{
					param->takeDamage(m_damage);
					kill();
					break;
				}
			}
		}
	}

} // function end