
#pragma region

#include <Windows.h>
#include <memory>
#include "D3DManager.h"
#include "SpriteManager.h"
#include "Recruit.h"
#include "StandardArrow.h"
#include "Sergent.h"
#include "General.h"
#include "Team.h"
#include "Bow.h"
#include <map>
#include "FancyBow.h"
#include "FancyArrow.h"
#include "LiveAmmoManager.h"
#include "Sword.h"
#include "RNG.h"
#include "Dagger.h"
#include "ThrowingDagger.h"
#include "ParticleManager.h"

#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")

//debugging tools
#include "imgui-master/imgui.h"
#include "imgui-master/imgui_impl_dx11.h"
//#include <dinput.h>

#define minHeight 80
#define minWidth 15
#define maxWidth 1254
#define maxHeight 937

double PCFreq = 0.0;
__int64 CounterStart = 0;

extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
LRESULT WINAPI WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE prevInstance, LPWSTR cmdLine, int cmdShow)
{

	void setUpTeam(Team& team);
	void startCounter();
	double getCounter();

	UNREFERENCED_PARAMETER(prevInstance);
	UNREFERENCED_PARAMETER(cmdLine);

	WNDCLASSEX wndClass = { 0 };
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hInstance = hInstance;
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = L"Simulation Engine Test";

	if (!RegisterClassEx(&wndClass))
		return -1;

#include "wtypes.h"
		RECT desktop;
		// Get a handle to the desktop window
		const HWND hDesktop = GetDesktopWindow();
		// Get the size of screen to the variable desktop
		GetWindowRect(hDesktop, &desktop);
		// The top left corner will have coordinates (0,0)
		// and the bottom right corner will have coordinates
		// (horizontal, vertical)
		int horizontal = desktop.right;
		int vertical = desktop.bottom;

	//RECT rc = { 0, 0, 1280, 700 };
	RECT rc = { 0, 0, horizontal, vertical };
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);

	HWND hwnd = CreateWindowA("Simulation Engine Test", "Simulation Engine Test", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance, NULL);

	if (!hwnd)
		return -1;

	D3DManager d3dManager;
	SpriteManager spriteManager;
	LiveAmmoManager liveAmmoManager;
	RNG rng;
	ParticleManager particleManager;

	ShowWindow(hwnd, SW_SHOWDEFAULT);
	UpdateWindow(hwnd);

	int totalNumSoldiers = 50;

	const float middleWidth = (maxWidth + minWidth) * 0.5;
	const float bufferZone = 50.0f;

	XMFLOAT2 teamMin = XMFLOAT2(middleWidth + bufferZone, minHeight);
	XMFLOAT2 teamMax = XMFLOAT2(maxWidth, maxHeight);
	Team t1(LEFT, teamMin, teamMax, totalNumSoldiers * 0.5);

	teamMin = XMFLOAT2(minWidth, minHeight);
	teamMax = XMFLOAT2(middleWidth - bufferZone, maxHeight);
	Team t2(RIGHT, teamMin, teamMax, totalNumSoldiers * 0.5);

	d3dManager.initialise(hInstance, hwnd);
	spriteManager.initialise();

	t1.setOtherTeam(&t2);
	t2.setOtherTeam(&t1);

	//setUpTeam(t1);
	//setUpTeam(t2);

	// Setup ImGui binding
	ImGui_ImplDX11_Init(hwnd, d3dManager.getD3DDevice(), d3dManager.getD3DContext());

	bool show_test_window = true;
	bool show_another_window = false;
	ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

	//t1.addRecruit(XMFLOAT3(1254.0f, 80.0f, 1.0f), new Bow(), new StandardArrow(L"Assets/Sprites/arrow_left_new.png", LEFT));
	//t1.addRecruit(XMFLOAT3(1254.0f, 936.0f, 1.0f), new Bow(), new StandardArrow(L"Assets/Sprites/arrow_left_new.png", LEFT));

	t2.addRecruit(XMFLOAT3(15.0f, 80.0f, 1.0f), new Bow(), new StandardArrow(L"Assets/Sprites/arrow_right_new.png", RIGHT));
	//t2.addRecruit(XMFLOAT3(15.0f, 937.0f, 1.0f), new Bow(), new StandardArrow(L"Assets/Sprites/arrow_right_new.png", RIGHT));

	/* if(t1.getDirectionFacing() == LEFT)
	{
		//t1.addRecruit(XMFLOAT3(60.0f, 95.0f, 1.0f), new Bow(), new StandardArrow(L"Assets/Sprites/arrow_left_new.png", LEFT));
		//t1.addRecruit(XMFLOAT3(900.0f, 700.0f, 1.0f), new Dagger(), new ThrowingDagger(L"Assets/Sprites/dagger_left_new.png", LEFT));
		//t1.addRecruit(XMFLOAT3(1000.0f, 700.0f, 1.0f), new Sword(), nullptr);
		//t1.addSergent(XMFLOAT3(1000.0f, 500.0f, 1.0f), new FancyBow(), new FancyArrow(L"Assets/Sprites/arrow_left_new.png", LEFT));
		//t1.addGeneral(XMFLOAT3(800.0f, 500.0f, 1.0f), new Bow(),  new StandardArrow(L"Assets/Sprites/arrow_left_new.png", LEFT));
	}

	if (t2.getDirectionFacing() == RIGHT)
	{
	//	t2.addRecruit(XMFLOAT3(25.0f, 95.0f, 1.0f), new Bow(), new StandardArrow(L"Assets/Sprites/arrow_right_new.png", RIGHT));
		//t2.addSergent(XMFLOAT3(500.0f, 700.0f, 1.0f), new FancyBow(), new FancyArrow(L"Assets/Sprites/arrow_right_new.png", RIGHT));
		//t2.addSergent(XMFLOAT3(600.0f, 300.0f, 1.0f), new FancyBow(), new FancyArrow(L"Assets/Sprites/arrow_right_new.png", RIGHT));
		//t2.addGeneral(XMFLOAT3(400.0f, 500.0f, 1.0f), new Bow(), new StandardArrow(L"Assets/Sprites/arrow_right_new.png", RIGHT));
	}  */
	
	LARGE_INTEGER oneFrame;
	QueryPerformanceFrequency(&oneFrame);
	oneFrame.QuadPart /= 60;

	// If there is more than sleepGap time left, sleep for 1ms.
	// (sleepGap itself is rather more than 1ms, because sleeping
	// isn't always very accurate.)
	//
	// This cuts down on CPU usage, so that other apps runs better,
	// laptop stays cool, etc.
	LARGE_INTEGER sleepGap;
	QueryPerformanceFrequency(&sleepGap);
	sleepGap.QuadPart /= 250;

	// This makes Sleep more accurate.
	timeBeginPeriod(1);

	// Time for next update.
	LARGE_INTEGER nextUpdate;
	QueryPerformanceCounter(&nextUpdate);
	nextUpdate.QuadPart += oneFrame.QuadPart;

	MSG msg = { 0 };

	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg); 
			continue;
		}

		ImGui_ImplDX11_NewFrame();

		float x = t2.getRecruits().at(0)->getSoldierSprite()->getPosition().x;
		float y = t2.getRecruits().at(0)->getSoldierSprite()->getPosition().y;
			
		// 1. Show a simple window.
		// Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets appears in a window automatically called "Debug".
		{
			ImGui::Begin("OO Version stats:");
			//ImGui::Text("OO Version stats:");
			ImGui::Text("Total Num Soldiers: %i", totalNumSoldiers);
			ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
			ImGui::SliderFloat("x pos", &x, -500.0f, 1500.0f);
			ImGui::SliderFloat("y pos", &y, -500.0f, 1500.0f);
			ImGui::End();
		}

		t2.getRecruits().at(0)->getSoldierSprite()->changePosition(XMFLOAT3(x, y, 1));

		// Wait until the next 60th-of-a-second boundary has
		// arrived (or been and gone).
		LARGE_INTEGER now;

		for (;;)
		{
			QueryPerformanceCounter(&now);
			LONGLONG delayTimeLeft = nextUpdate.QuadPart - now.QuadPart;
			if (delayTimeLeft <= 0)
				break;

			if (delayTimeLeft >= sleepGap.QuadPart)
				Sleep(1);
		}
		nextUpdate.QuadPart = now.QuadPart + oneFrame.QuadPart;

		startCounter();
		liveAmmoManager.updateLiveAmmo();
		t1.updateSoldiers();
		t2.updateSoldiers();
		particleManager.updateParticles();
		spriteManager.updateSprites();

		float clearColour[4] = { 0.0f, 0.0f, 0.25f, 1.0f };
		d3dManager.getD3DContext()->ClearRenderTargetView(d3dManager.getBackBufferTarget(), clearColour);

		spriteManager.renderSprites();
		ImGui::Render();

		D3DManager::s_d3dManager->getSwapChain()->Present(0, 0);
		
		double result = getCounter();
		//int i = 0;
	}

	ImGui_ImplDX11_Shutdown();
	t1.cleanUpTeam();
	t2.cleanUpTeam();
	return static_cast<int>(msg.wParam);
}

LRESULT WINAPI WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//PAINTSTRUCT paintStruct;
	//HDC hDC;

	if (ImGui_ImplWin32_WndProcHandler(hwnd, message, wParam, lParam))
		return true;

	switch (message)
	{

	case WM_SIZE:
		if (D3DManager::s_d3dManager)
		{
			if (D3DManager::s_d3dManager->getD3DDevice() != NULL && wParam != SIZE_MINIMIZED)
			{
				ImGui_ImplDX11_InvalidateDeviceObjects();
				ImGui_ImplDX11_CreateDeviceObjects();
			}
		}
		return 0;
	//case WM_PAINT:
	//	hDC = BeginPaint(hwnd, &paintStruct);
	//	EndPaint(hwnd, &paintStruct);
	//	break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hwnd, message, wParam, lParam);
	}
	return 0;
}

void setUpTeam(Team& team)
{
	Weapon* generateWeapon(const int& weaponID);
	Ammunition* generateAmmo(const int& ammoID, const int direction);

	float recruitProp(0.6f), sergentProp(0.3f), generalProp(0.1f);
	int numRecruits = team.getNumStartingSoldiers() * recruitProp;
	int numSergents = team.getNumStartingSoldiers() * sergentProp;
	int numGenerals = team.getNumStartingSoldiers() * generalProp;

	int weaponOrAmmoVal;
	float posX, posY;

	Weapon* tempWeaponPointer = nullptr;
	Ammunition* tmepAmmoPointer = nullptr;

	for (int i = 0; i < numRecruits; ++i)
	{
		// create a weapon
		weaponOrAmmoVal = RNG::s_RNG->generateRandomNumber(0, 3); // hardCoded because currently there 4 weapons but not a way to calculate this
		tempWeaponPointer = generateWeapon(weaponOrAmmoVal);
		
		// create the ammo based on weapon
		if(weaponOrAmmoVal < 2)
			weaponOrAmmoVal = RNG::s_RNG->generateRandomNumber(0, 1); // some sort of bow therefore generate arrows
		else if(weaponOrAmmoVal == 2)
			weaponOrAmmoVal = 4; // sword so we need to have nullptr as our ammo
		else
			weaponOrAmmoVal = 2; // throwing weapon so generate something throwable
		
		tmepAmmoPointer = generateAmmo(weaponOrAmmoVal, team.getDirectionFacing());

		// create the position
		posX = RNG::s_RNG->generateRandomNumber(team.getMinArea().x, team.getMaxArea().x);
		posY = RNG::s_RNG->generateRandomNumber(team.getMinArea().y, team.getMaxArea().y);

		team.addRecruit(XMFLOAT3(posX, posY, 1), tempWeaponPointer, tmepAmmoPointer);
	}

	for (int i = 0; i < numSergents; ++i)
	{
		// create a weapon
		weaponOrAmmoVal = RNG::s_RNG->generateRandomNumber(0, 3); // hardCoded because currently there 4 weapons but not a way to calculate this
		tempWeaponPointer = generateWeapon(weaponOrAmmoVal);

		// create the ammo based on weapon
		if (weaponOrAmmoVal < 2)
			weaponOrAmmoVal = RNG::s_RNG->generateRandomNumber(0, 1); // some sort of bow therefore generate arrows
		else if (weaponOrAmmoVal == 2)
			weaponOrAmmoVal = 4; // sword so we need to have nullptr as our ammo
		else
			weaponOrAmmoVal = 2; // throwing weapon so generate something throwable

		tmepAmmoPointer = generateAmmo(weaponOrAmmoVal, team.getDirectionFacing());

		// create the position
		posX = RNG::s_RNG->generateRandomNumber(team.getMinArea().x, team.getMaxArea().x);
		posY = RNG::s_RNG->generateRandomNumber(team.getMinArea().y, team.getMaxArea().y);

		team.addSergent(XMFLOAT3(posX, posY, 1), tempWeaponPointer, tmepAmmoPointer);
	}

	for (int i = 0; i < numGenerals; ++i)
	{
		// create a weapon
		weaponOrAmmoVal = RNG::s_RNG->generateRandomNumber(0, 3); // hardCoded because currently there 4 weapons but not a way to calculate this
		tempWeaponPointer = generateWeapon(weaponOrAmmoVal);

		// create the ammo based on weapon
		if (weaponOrAmmoVal < 2)
			weaponOrAmmoVal = RNG::s_RNG->generateRandomNumber(0, 1); // some sort of bow therefore generate arrows
		else if (weaponOrAmmoVal == 2)
			weaponOrAmmoVal = 4; // sword so we need to have nullptr as our ammo
		else
			weaponOrAmmoVal = 2; // throwing weapon so generate something throwable

		tmepAmmoPointer = generateAmmo(weaponOrAmmoVal, team.getDirectionFacing());

		// create the position
		posX = RNG::s_RNG->generateRandomNumber(team.getMinArea().x, team.getMaxArea().x);
		posY = RNG::s_RNG->generateRandomNumber(team.getMinArea().y, team.getMaxArea().y);

		team.addGeneral(XMFLOAT3(posX, posY, 1), tempWeaponPointer, tmepAmmoPointer);
	}
	
	tempWeaponPointer = nullptr;
	tmepAmmoPointer = nullptr;
}

Weapon* generateWeapon(const int& weaponID)
{
	Weapon* tempWeaponPointer = nullptr;
	switch (weaponID)
	{
	case 0: // bow
	{
		tempWeaponPointer = new Bow();
	}
	break;

	case 1: // fancy bow
	{
		tempWeaponPointer = new FancyBow();
	}
	break;

	case 2: // sword
	{
		tempWeaponPointer = new Sword();
	}
	break;

	case 3: // dagger
	{
		tempWeaponPointer = new Dagger();
	}
	break;
	
	default:
		break;
	}

	return tempWeaponPointer;
}

Ammunition* generateAmmo(const int& ammoID, const int direction)
{
	Ammunition* tempAmmoPointer = nullptr;

	switch (ammoID)
	{
	case 0: // standard arrow
		{
		if (direction == 0) // left
			tempAmmoPointer = new StandardArrow(L"Assets/Sprites/arrow_left_new.png", 0);
		else
			tempAmmoPointer = new StandardArrow(L"Assets/Sprites/arrow_right_new.png", 1);
		}
		break;

	case 1: // fancy arrow
		{
		if (direction == 0) // left
			tempAmmoPointer = new FancyArrow(L"Assets/Sprites/arrow_left_new.png", 0);
		else
			tempAmmoPointer = new FancyArrow(L"Assets/Sprites/arrow_right_new.png", 1);
		}
		break;

	case 2: // throwing Dagger
		{
		if (direction == 0) // left
			tempAmmoPointer = new ThrowingDagger(L"Assets/Sprites/dagger_left_new.png", 0);
		else
			tempAmmoPointer = new ThrowingDagger(L"Assets/Sprites/dagger_right_new.png", 1);
		}
		break;
	default:
		break;
	}

	return tempAmmoPointer;
}

void startCounter()
{
	LARGE_INTEGER li;
	QueryPerformanceFrequency(&li);
		
	PCFreq = double(li.QuadPart);

	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}

double getCounter()
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return double(li.QuadPart - CounterStart) / PCFreq;
}

#pragma endregion