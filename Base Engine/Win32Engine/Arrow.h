#pragma once

#include "Ammunition.h"

class Arrow : public Ammunition
{
protected:

public:
	Arrow(LPWSTR weaponSpriteName, const float& lifeTime, const float& damage, const float& speed, const int& direction);
	~Arrow();
};