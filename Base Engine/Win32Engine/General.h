#pragma once

#include "Soldier.h"

class General : public Soldier
{
private:

public:
	General(const XMFLOAT3& pos, Weapon* weapon, Ammunition* ammo, LPWSTR fileName, Team* team);
	virtual ~General();

	virtual void behave();

	void update();
	void render();
};
