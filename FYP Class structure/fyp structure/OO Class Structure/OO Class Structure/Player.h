#pragma once
#include <vector>
#include "Soldier.h"
#include "Sound.h"
#include "Behaviour.h"
#include "Ammunition.h"
#include "Weapon.h"
#include "PowerUp.h"
class Player
{
private:
	std::string m_playerName;
	int m_score;
	int m_previousScore;
	Soldier* m_soldier;
	std::vector<float> m_pos;
	std::vector<float> m_prevPos;
	Sound* m_playerVoice;
	Behaviour m_playerBehaviour;
	Ammunition* m_playerAmmunition;
	Weapon* m_weapon;
	PowerUp m_powerUp;

public:
	Player();
	~Player();


};