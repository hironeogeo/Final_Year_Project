#pragma once

#include "Weapon.h"

class Ranged : public virtual Weapon
{
protected:
	float m_launchFactor;
	int m_shotsFired;
public:
	Ranged(const float& damage, const float& speed, const int& refrershTime);
	~Ranged();

	void attack() override;

	void update();
	void render();

	void incrementShotsFired();
	int getShotsFired() const;
};
