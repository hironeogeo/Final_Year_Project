#include "Sprite.h"
#include "D3DManager.h"

//#include "D3DManager.h"

Sprite::Sprite(LPWSTR spritePath)
	: m_position(XMFLOAT3(0.0f,0.0f, 1.0f)), m_scale(XMFLOAT3(1.0f, 1.0f, 1.0f)), m_rotation(0.0f), m_isVisible(true), m_worldMatrix(XMMATRIX(XMMatrixIdentity())),
	m_spriteColourMap(0), m_spriteVertexBuffer(0)
{
	loadContent(spritePath);
	m_bs.m_parent = this;
}

Sprite::Sprite(LPWSTR spritePath, const XMFLOAT3& position)
	: m_position(position), m_scale(XMFLOAT3(1.0f, 1.0f, 1.0f)), m_rotation(0.0f), m_isVisible(true), m_worldMatrix(XMMATRIX(XMMatrixIdentity())),
	m_spriteColourMap(0), m_spriteVertexBuffer(0)
{
	loadContent(spritePath);
	m_bs.m_parent = this;
}

Sprite::Sprite(LPWSTR spritePath, const XMFLOAT3& position, const XMFLOAT3& scale, const float& rotation)
	: m_position(position), m_scale(scale), m_rotation(rotation), m_spriteColourMap(0), m_spriteVertexBuffer(0), m_worldMatrix(XMMATRIX(XMMatrixIdentity()))
{
	loadContent(spritePath);
	m_bs.m_parent = this;
}

bool Sprite::loadContent(LPWSTR spritePath)
{
	HRESULT d3dResult;

	// load in the sprite image
	d3dResult = D3DX11CreateShaderResourceViewFromFile(D3DManager::s_d3dManager->getD3DDevice(), spritePath, 0, 0, &m_spriteColourMap, 0);

	if (FAILED(d3dResult))
	{
		DXTRACE_MSG(L"Failed to load the texture image!");
		return false;
	}

	// setup the colour texture
	ID3D11Resource* colorTex;
	m_spriteColourMap->GetResource(&colorTex);

	D3D11_TEXTURE2D_DESC colorTexDesc;
	((ID3D11Texture2D*)colorTex)->GetDesc(&colorTexDesc);
	colorTex->Release();

	m_bs.m_spriteWidth = colorTexDesc.Width;

	float halfWidth = (float)colorTexDesc.Width / 2.0f;
	float halfHeight = (float)colorTexDesc.Height / 2.0f;

	// setup the verticies
	VertexPos vertices[] =
	{
		{ XMFLOAT3(halfWidth,  halfHeight, 1.0f), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(halfWidth, -halfHeight, 1.0f), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(-halfWidth, -halfHeight, 1.0f), XMFLOAT2(0.0f, 1.0f) },

		{ XMFLOAT3(-halfWidth, -halfHeight, 1.0f), XMFLOAT2(0.0f, 1.0f) },
		{ XMFLOAT3(-halfWidth,  halfHeight, 1.0f), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(halfWidth,  halfHeight, 1.0f), XMFLOAT2(1.0f, 0.0f) },
	};

	D3D11_BUFFER_DESC vertexDesc;
	ZeroMemory(&vertexDesc, sizeof(vertexDesc));
	vertexDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexDesc.ByteWidth = sizeof(VertexPos) * 6;

	D3D11_SUBRESOURCE_DATA resourceData;
	ZeroMemory(&resourceData, sizeof(resourceData));
	resourceData.pSysMem = vertices;

	// setup the sprite vertex buffer
	d3dResult = D3DManager::s_d3dManager->getD3DDevice()->CreateBuffer(&vertexDesc, &resourceData, &m_spriteVertexBuffer);

	if (FAILED(d3dResult))
	{
		DXTRACE_MSG(L"Failed to create vertex buffer!");
		return false;
	}

	return true;
}

Sprite::~Sprite()
{
	if (m_spriteVertexBuffer)
		m_spriteVertexBuffer->Release();
	
	if (m_spriteColourMap)
		m_spriteColourMap->Release();
	
	m_spriteVertexBuffer = nullptr;
	m_spriteColourMap = nullptr;
}

void Sprite::update()
{
	XMMATRIX trans, rot, scale;

	XMFLOAT4 tmpFloat = XMFLOAT4(m_position.x, m_position.y, 1.0f, 1.0f);
	trans = (XMMatrixTranslationFromVector(XMLoadFloat4(&tmpFloat)));
	scale = XMMatrixScaling(m_scale.x, m_scale.y, 1.0f);
	rot = XMMatrixRotationY(XMConvertToRadians(m_rotation));
	m_worldMatrix = scale * rot * trans * XMMatrixIdentity();

	m_bs.m_position = m_worldMatrix.r[3];
	XMFLOAT4 tmpFloatForCalc;
	XMStoreFloat4(&tmpFloatForCalc, m_worldMatrix.r[3]);
	tmpFloatForCalc.x - m_bs.m_spriteWidth;
	m_bs.m_leftEdgeBound = XMLoadFloat4(&tmpFloatForCalc);

	XMStoreFloat4(&tmpFloatForCalc, m_worldMatrix.r[3]);
	tmpFloatForCalc.x + m_bs.m_spriteWidth;
	m_bs.m_rightEdgeBound = XMLoadFloat4(&tmpFloatForCalc);
}

void Sprite::render()
{
	unsigned int stride = sizeof(Sprite::VertexPos);
	unsigned int offset = 0;

	D3DManager::s_d3dManager->getD3DContext()->Draw(6, 0);
}

void Sprite::changePosition(const XMFLOAT3& newPos)
{
	m_position = newPos;
}

void Sprite::changeRotation(const float& newRot)
{
	m_rotation = newRot;
}

void Sprite::changeScale(const XMFLOAT3& newScale)
{
	m_scale = newScale;
}

void Sprite::changeVisibility(bool newVisibility)
{
	m_isVisible = newVisibility;
}

void Sprite::toggleVisibility()
{
	m_isVisible = !m_isVisible;
}

XMFLOAT3 Sprite::getPosition()
{
	return m_position;
}

XMFLOAT3 Sprite::getScale()
{
	return m_scale;
}

float Sprite::getRotation()
{
	return m_rotation;
}

bool Sprite::getVisibility()
{
	return m_isVisible;
}

XMMATRIX Sprite::getWorldMatrix() const
{
	return m_worldMatrix;
}

ID3D11Buffer* Sprite::getVertexBuffer() const
{
	return m_spriteVertexBuffer;
}

ID3D11ShaderResourceView* Sprite::getShaderResource() const
{
	return m_spriteColourMap;
}

BoundingSprite* Sprite::getBoundingSprite()
{
	return &m_bs;
}

void Sprite::reset(XMFLOAT3 pos)
{
	m_position = pos;
}
