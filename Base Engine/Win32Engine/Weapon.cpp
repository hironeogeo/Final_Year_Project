#include "Weapon.h"
#include "SpriteManager.h"
#include "Soldier.h"

Weapon::Weapon(const float& damage, const int& refreshTime, Soldier* owner = nullptr)
	: m_damage(damage), m_refreshTime(refreshTime)
{
	m_lastUsed = Time();
	m_tmp = m_lastUsed;
	m_refreshTime = refreshTime;

	m_tmp.setSeconds(m_tmp.getSeconds() + m_refreshTime);	
	m_hasRefereshed = true;
	m_diff = 0;
	m_fireOffset = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_owner = owner;
}

Weapon::~Weapon()
{
}

bool Weapon::getHasRefreshed() const
{
	return m_hasRefereshed;
}

void Weapon::update()
{
	m_tmp.setToCurrentTime();
	m_diff = Time(m_tmp - m_lastUsed).getSeconds();
	//m_diff >= m_refreshTime ? m_hasRefereshed = true : m_hasRefereshed = false;

	if (m_diff >= m_refreshTime)
	{
		m_hasRefereshed = true;
	}
	else 
		m_hasRefereshed = false;
}

XMFLOAT3 Weapon::getFiringOffset() const
{
	return m_fireOffset;
}

// should this be moved to the ranged weapon? 
// it's function is quite literally to work out where the live ammo should start!
void Weapon::calculateFiringOffset(const XMFLOAT3& pos, const int& direction)
{
	// apply the offset to the postiion variable so we get it spawning in the right place

	if (direction == 0) // left
		m_fireOffset = XMFLOAT3(pos.x - 75.0f, pos.y - 20.0f, pos.z);
	else
		m_fireOffset = XMFLOAT3(pos.x + 75.0f, pos.y - 20.0f, pos.z);

	updateLastUse();
} 

void Weapon::updateLastUse()
{
	m_lastUsed.setToCurrentTime();
}

void Weapon::setParent(Soldier* parent)
{
	m_owner = parent;
}