#pragma once

class Util
{
private:

public:
	Util();
	~Util();
	// returns true if the answer shoudl be a float after dividing
	bool checkFloat(const int& num1, const int& num2);
	int calculateTimeToAdd(int num1, const int& boundry, int& remainder);
	int calculateTimeToMinus(int num1, const int& boundry, int& remainder);
};