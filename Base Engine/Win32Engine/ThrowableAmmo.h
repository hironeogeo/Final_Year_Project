#pragma once

#include "Ammunition.h"

class ThrowableAmmo : public Ammunition
{
protected:

public:
	ThrowableAmmo(LPWSTR weaponSpriteName, const float& lifeTime, const float& damage, const float& speed, const int& direction);
	~ThrowableAmmo();
};
